<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/var/www/multisite_2020/wp-content/themes/g5_hydrogen/custom/config/leer/index.yaml',
    'modified' => 1604826310,
    'data' => [
        'name' => 'leer',
        'timestamp' => 1604826310,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1603348134
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'main' => 'Main',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'content' => [
                'system-content-4144' => 'Page Content'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
