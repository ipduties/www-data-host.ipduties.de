<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/var/www/multisite_2020/wp-content/themes/g5_hydrogen/custom/config/_error/index.yaml',
    'modified' => 1603348188,
    'data' => [
        'name' => '_error',
        'timestamp' => 1603348188,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1603348134
        ],
        'positions' => [
            'header' => 'Header',
            'breadcrumbs' => 'Breadcrumbs',
            'footer' => 'Footer'
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'main' => 'Main',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-9856' => 'Logo'
            ],
            'position' => [
                'position-header' => 'Header',
                'position-breadcrumbs' => 'Breadcrumbs',
                'position-footer' => 'Footer'
            ],
            'menu' => [
                'menu-8981' => 'Menu'
            ],
            'messages' => [
                'system-messages-1553' => 'System Messages'
            ],
            'content' => [
                'system-content-2998' => 'Page Content'
            ],
            'copyright' => [
                'copyright-5362' => 'Copyright'
            ],
            'spacer' => [
                'spacer-6762' => 'Spacer'
            ],
            'branding' => [
                'branding-1687' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-7795' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
