<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/var/www/multisite_2020/wp-content/themes/g5_hydrogen/custom/config/default/page/head.yaml',
    'modified' => 1604387542,
    'data' => [
        'meta' => [
            
        ],
        'head_bottom' => '',
        'atoms' => [
            0 => [
                'id' => 'assets-1020',
                'type' => 'assets',
                'title' => 'Custom CSS / JS',
                'attributes' => [
                    'enabled' => '1',
                    'css' => [
                        0 => [
                            'location' => '',
                            'inline' => 'body {
background-image: url("https://i.imgur.com/AEybLRo.jpg")!important;
background-repeat: repeat-y;
background-size: 100% auto;
}

#g-page-surround {
background: transparent;
}',
                            'extra' => [
                                
                            ],
                            'priority' => '10',
                            'name' => 'custom01'
                        ]
                    ],
                    'javascript' => [
                        
                    ]
                ]
            ]
        ]
    ]
];
