<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/var/www/multisite_2020/wp-content/themes/g5_hydrogen/custom/config/_error/layout.yaml',
    'modified' => 1603348188,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1603348134
        ],
        'layout' => [
            '/header/' => [
                0 => [
                    0 => 'logo-9856 30',
                    1 => 'position-header 70'
                ]
            ],
            '/navigation/' => [
                0 => [
                    0 => 'menu-8981'
                ]
            ],
            '/main/' => [
                0 => [
                    0 => 'position-breadcrumbs'
                ],
                1 => [
                    0 => 'system-messages-1553'
                ],
                2 => [
                    0 => 'system-content-2998'
                ]
            ],
            '/footer/' => [
                0 => [
                    0 => 'position-footer'
                ],
                1 => [
                    0 => 'copyright-5362 40',
                    1 => 'spacer-6762 30',
                    2 => 'branding-1687 30'
                ]
            ],
            'offcanvas' => [
                0 => [
                    0 => 'mobile-menu-7795'
                ]
            ]
        ],
        'structure' => [
            'header' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'navigation' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'main' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'footer' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ]
        ],
        'content' => [
            'position-header' => [
                'attributes' => [
                    'key' => 'header'
                ]
            ],
            'position-breadcrumbs' => [
                'attributes' => [
                    'key' => 'breadcrumbs'
                ]
            ],
            'position-footer' => [
                'attributes' => [
                    'key' => 'footer'
                ]
            ]
        ]
    ]
];
