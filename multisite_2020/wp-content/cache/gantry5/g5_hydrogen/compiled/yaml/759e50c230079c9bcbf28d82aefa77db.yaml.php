<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/var/www/multisite_2020/wp-content/themes/g5_hydrogen/custom/config/ip_duties_home/layout.yaml',
    'modified' => 1603671294,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/home.png',
            'name' => 'home',
            'timestamp' => 1603348134
        ],
        'layout' => [
            '/header/' => [
                0 => [
                    0 => 'logo-1070'
                ]
            ],
            '/navigation/' => [
                0 => [
                    0 => 'menu-5588 50'
                ]
            ],
            '/showcase/' => [
                
            ],
            '/feature/' => [
                
            ],
            '/main/' => [
                0 => [
                    0 => 'system-messages-9294'
                ],
                1 => [
                    0 => 'sample-2'
                ],
                2 => [
                    0 => 'system-content-1913'
                ]
            ],
            '/subfeature/' => [
                0 => [
                    0 => 'sample-3'
                ]
            ],
            '/footer/' => [
                0 => [
                    0 => 'position-footer'
                ],
                1 => [
                    0 => 'copyright-6000 33.3',
                    1 => 'social-4975 33.3',
                    2 => 'branding-9184 33.3'
                ]
            ],
            'offcanvas' => [
                0 => [
                    0 => 'mobile-menu-3024'
                ]
            ]
        ],
        'structure' => [
            'header' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'navigation' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '',
                    'class' => '',
                    'variations' => ''
                ]
            ],
            'showcase' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => '',
                    'variations' => ''
                ]
            ],
            'feature' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'main' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'subfeature' => [
                'type' => 'section',
                'attributes' => [
                    'class' => 'flush',
                    'boxed' => ''
                ]
            ],
            'footer' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'offcanvas' => [
                'attributes' => [
                    'position' => 'g-offcanvas-right',
                    'class' => '',
                    'extra' => [
                        
                    ],
                    'swipe' => '1',
                    'css3animation' => '1'
                ]
            ]
        ],
        'content' => [
            'logo-1070' => [
                'title' => 'Logo / Image',
                'attributes' => [
                    'url' => '',
                    'target' => '_self',
                    'image' => 'https://i.imgur.com/xqa4eSW.png',
                    'link' => '1',
                    'svg' => '',
                    'text' => 'IP duties UG',
                    'class' => 'logo'
                ],
                'block' => [
                    'variations' => 'nomarginall'
                ]
            ],
            'menu-5588' => [
                'attributes' => [
                    'enabled' => 0,
                    'menu' => '',
                    'base' => '49',
                    'startLevel' => '1',
                    'maxLevels' => '0',
                    'renderTitles' => '0',
                    'hoverExpand' => '1',
                    'mobileTarget' => '0',
                    'forceTarget' => '0'
                ],
                'block' => [
                    'fixed' => '1'
                ]
            ],
            'sample-2' => [
                'title' => 'Getting Started',
                'attributes' => [
                    'enabled' => 0,
                    'image' => '',
                    'headline' => '',
                    'description' => '<h1 class="center">Getting Started</h1>
<div class="device-promo"><div class="macbook"><div class="macbook-color"></div></div><div class="ipad"><div class="ipad-color"></div></div><div class="iphone"><div class="iphone-color"></div></div></div><p>Welcome to Gantry 5 featuring <strong>Hydrogen</strong>, the first theme built on the Gantry 5 framework. If you want to get started, the best way is to navigate to the Gantry Administrator via your platform\'s Administration panel.</p>

<p>Once you are in the Gantry 5 Administrator, you will be able to edit virtually every aspect of your site from its <strong>Layout</strong> to its <strong>Style</strong>. You can even refine how the menus appear using Gantry 5’s new <strong>Menu Editor</strong>.</p>

<p>You can set different style preferences for different pages, and have them assigned accordingly using the <strong>Assignments</strong> administrative panel.</p>

<div class="info-box"><div class="fa fa-graduation-cap float-left"></div><p>Look for more information on Gantry 5 in our documentation, and stay tuned to the RocketTheme Blog for more information on new features and development updates as development continues.</p>

<p><a href="http://docs.gantry.org" class="button">Learn More</a></p></div>

<h1 class="center">How to Contribute</h1>

<div class="g-grid">
<div class="g-block size-37"><p>Thank you for using Gantry 5 and the Hydrogen theme. We welcome you to contribute to the project by submitting bug reports through <strong>GitHub</strong>, and/or submit your own code changes to the <strong>Gantry 5 project</strong> for consideration.</p>
<p><a href="https://github.com/gantry/gantry5" class="button">Gantry 5 on GitHub</a></p>
</div>
<div class="g-block size-26 middle"><div class="fa fa-github-square"></div></div>

<div class="g-block size-37"><p>If you would like to assist in creating documentation for Gantry 5, you can do so through the <strong>Gantry 5 Documentation</strong> project on <strong>GitHub</strong>.</p>
<p><a href="https://github.com/gantry/docs" class="button">Gantry Docs on GitHub</a></p>
</div>
</div>

<p>Once again, thank you for participating. We hope you enjoy testing Gantry 5 every bit as much as we have enjoyed creating it.</p>',
                    'link' => '',
                    'linktext' => '',
                    'samples' => [
                        
                    ]
                ]
            ],
            'sample-3' => [
                'title' => 'Key Features',
                'attributes' => [
                    'enabled' => 0,
                    'image' => '',
                    'headline' => 'Key Features',
                    'description' => '<p>Gantry 5 is packed full of features created to empower the development of designs into fully functional layouts with the absolute minimum effort and fuss</p>',
                    'link' => '',
                    'linktext' => '',
                    'samples' => [
                        0 => [
                            'icon' => 'fa fa-code',
                            'subtitle' => '',
                            'description' => '<p>Gantry 5 leverages the power of <a href="http://twig.sensiolabs.org/">Twig</a> to make creating powerful, dynamic themes quick and easy.</p>',
                            'title' => 'Twig Templating'
                        ],
                        1 => [
                            'icon' => 'fa fa-newspaper-o',
                            'subtitle' => '',
                            'description' => '<p>Drag-and-drop functionality gives you the power to place content blocks, resize them, and configure their unique settings in seconds.</p>',
                            'title' => 'Layout Manager'
                        ],
                        2 => [
                            'icon' => 'fa fa-cubes',
                            'subtitle' => '',
                            'description' => '<p>Create, configure, and manage content blocks as well as special features and functionality with the powerful particle system.</p>',
                            'title' => 'Particles System'
                        ]
                    ]
                ]
            ],
            'position-footer' => [
                'attributes' => [
                    'key' => 'footer'
                ]
            ],
            'social-4975' => [
                'attributes' => [
                    'enabled' => 0,
                    'css' => [
                        'class' => 'social-items'
                    ],
                    'items' => [
                        0 => [
                            'icon' => 'fa fa-twitter',
                            'text' => 'Twitter',
                            'link' => 'http://twitter.com/rockettheme',
                            'name' => 'Twitter'
                        ],
                        1 => [
                            'icon' => 'fa fa-facebook',
                            'text' => 'Facebook',
                            'link' => 'http://facebook.com/rockettheme',
                            'name' => 'Facebook'
                        ],
                        2 => [
                            'icon' => 'fa fa-google',
                            'text' => 'Google',
                            'link' => 'http://plus.google.com/+rockettheme',
                            'name' => 'Google'
                        ],
                        3 => [
                            'icon' => 'fa fa-rss',
                            'text' => 'RSS',
                            'link' => 'http://www.rockettheme.com/product-updates?rss',
                            'name' => 'RSS'
                        ]
                    ]
                ],
                'block' => [
                    'variations' => 'center'
                ]
            ],
            'branding-9184' => [
                'attributes' => [
                    'enabled' => 0
                ],
                'block' => [
                    'variations' => 'align-right'
                ]
            ],
            'mobile-menu-3024' => [
                'title' => 'Mobile Menu',
                'attributes' => [
                    'enabled' => 0
                ]
            ]
        ]
    ]
];
