<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/var/www/multisite_2020/wp-content/themes/g5_hydrogen/custom/config/_body_only/index.yaml',
    'modified' => 1603348188,
    'data' => [
        'name' => '_body_only',
        'timestamp' => 1603348188,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1603348134
        ],
        'positions' => [
            'header' => 'Header',
            'breadcrumbs' => 'Breadcrumbs',
            'footer' => 'Footer'
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'main' => 'Main',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-8952' => 'Logo'
            ],
            'position' => [
                'position-header' => 'Header',
                'position-breadcrumbs' => 'Breadcrumbs',
                'position-footer' => 'Footer'
            ],
            'menu' => [
                'menu-4176' => 'Menu'
            ],
            'messages' => [
                'system-messages-1087' => 'System Messages'
            ],
            'content' => [
                'system-content-4520' => 'Page Content'
            ],
            'copyright' => [
                'copyright-9563' => 'Copyright'
            ],
            'spacer' => [
                'spacer-9814' => 'Spacer'
            ],
            'branding' => [
                'branding-9226' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-9138' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
