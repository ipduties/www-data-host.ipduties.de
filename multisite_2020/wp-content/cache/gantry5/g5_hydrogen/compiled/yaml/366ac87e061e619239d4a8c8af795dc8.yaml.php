<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/var/www/multisite_2020/wp-content/themes/g5_hydrogen/custom/config/default/layout.yaml',
    'modified' => 1604387719,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1603348134
        ],
        'layout' => [
            '/header/' => [
                0 => [
                    0 => 'logo-9209 50',
                    1 => 'position-header 50'
                ]
            ],
            '/navigation/' => [
                0 => [
                    0 => 'menu-6653'
                ]
            ],
            '/main/' => [
                0 => [
                    0 => 'position-breadcrumbs'
                ],
                1 => [
                    0 => 'system-messages-2907'
                ],
                2 => [
                    0 => 'system-content-3375'
                ]
            ],
            '/footer/' => [
                0 => [
                    0 => 'position-footer'
                ],
                1 => [
                    0 => 'menu-2482'
                ],
                2 => [
                    0 => 'copyright-2589 40',
                    1 => 'spacer-7587 30',
                    2 => 'branding-1357 30'
                ]
            ],
            'offcanvas' => [
                0 => [
                    0 => 'mobile-menu-5246'
                ]
            ]
        ],
        'structure' => [
            'header' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'navigation' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'main' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'footer' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ]
        ],
        'content' => [
            'logo-9209' => [
                'attributes' => [
                    'image' => 'https://i.imgur.com/xqa4eSW.png',
                    'svg' => '',
                    'text' => 'IP duties UG',
                    'class' => ''
                ]
            ],
            'position-header' => [
                'attributes' => [
                    'enabled' => 0,
                    'key' => 'header'
                ]
            ],
            'menu-6653' => [
                'attributes' => [
                    'enabled' => 0
                ]
            ],
            'position-breadcrumbs' => [
                'attributes' => [
                    'key' => 'breadcrumbs'
                ]
            ],
            'position-footer' => [
                'attributes' => [
                    'key' => 'footer'
                ]
            ],
            'menu-2482' => [
                'attributes' => [
                    'menu' => 'footer'
                ]
            ],
            'branding-1357' => [
                'attributes' => [
                    'enabled' => 0
                ]
            ],
            'mobile-menu-5246' => [
                'attributes' => [
                    'enabled' => 0
                ]
            ]
        ]
    ]
];
