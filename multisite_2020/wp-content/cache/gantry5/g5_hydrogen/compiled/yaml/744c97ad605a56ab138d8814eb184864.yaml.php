<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/var/www/multisite_2020/wp-content/themes/g5_hydrogen/custom/config/home/index.yaml',
    'modified' => 1603348196,
    'data' => [
        'name' => 'home',
        'timestamp' => 1603348188,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/home.png',
            'name' => 'home',
            'timestamp' => 1603348134
        ],
        'positions' => [
            'footer' => 'Footer'
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'feature' => 'Feature',
            'subfeature' => 'Subfeature',
            'main' => 'Main',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'menu' => [
                'menu-4737' => 'Menu'
            ],
            'sample' => [
                'sample-1' => 'Gantry 5',
                'sample-2' => 'Getting Started',
                'sample-3' => 'Key Features'
            ],
            'messages' => [
                'system-messages-9294' => 'System Messages'
            ],
            'position' => [
                'position-footer' => 'Footer'
            ],
            'copyright' => [
                'copyright-6000' => 'Copyright'
            ],
            'social' => [
                'social-4975' => 'Social'
            ],
            'branding' => [
                'branding-9184' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-3024' => 'Mobile Menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
