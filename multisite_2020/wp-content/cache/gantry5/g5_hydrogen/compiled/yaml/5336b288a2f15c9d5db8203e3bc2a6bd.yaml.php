<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/var/www/multisite_2020/wp-content/themes/g5_hydrogen/config/default/particles/logo.yaml',
    'modified' => 1603348134,
    'data' => [
        'enabled' => '1',
        'url' => '',
        'image' => 'gantry-assets://images/gantry5-logo.png',
        'text' => 'Gantry 5',
        'class' => 'gantry-logo'
    ]
];
