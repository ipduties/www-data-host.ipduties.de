<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/var/www/multisite_2020/wp-content/themes/g5_hydrogen/custom/config/default/index.yaml',
    'modified' => 1604387719,
    'data' => [
        'name' => 'default',
        'timestamp' => 1604387719,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1603348134
        ],
        'positions' => [
            'header' => 'Header',
            'breadcrumbs' => 'Breadcrumbs',
            'footer' => 'Footer'
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'main' => 'Main',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-9209' => 'Logo'
            ],
            'position' => [
                'position-header' => 'Header',
                'position-breadcrumbs' => 'Breadcrumbs',
                'position-footer' => 'Footer'
            ],
            'menu' => [
                'menu-6653' => 'Menu',
                'menu-2482' => 'Menu'
            ],
            'messages' => [
                'system-messages-2907' => 'System Messages'
            ],
            'content' => [
                'system-content-3375' => 'Page Content'
            ],
            'copyright' => [
                'copyright-2589' => 'Copyright'
            ],
            'spacer' => [
                'spacer-7587' => 'Spacer'
            ],
            'branding' => [
                'branding-1357' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-5246' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
