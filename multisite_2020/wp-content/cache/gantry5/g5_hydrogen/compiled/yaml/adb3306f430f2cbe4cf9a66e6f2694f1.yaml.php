<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/var/www/multisite_2020/wp-content/themes/g5_hydrogen/custom/config/_offline/index.yaml',
    'modified' => 1603348188,
    'data' => [
        'name' => '_offline',
        'timestamp' => 1603348188,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1603348134
        ],
        'positions' => [
            'header' => 'Header',
            'breadcrumbs' => 'Breadcrumbs',
            'footer' => 'Footer'
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'main' => 'Main',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-2385' => 'Logo'
            ],
            'position' => [
                'position-header' => 'Header',
                'position-breadcrumbs' => 'Breadcrumbs',
                'position-footer' => 'Footer'
            ],
            'menu' => [
                'menu-7633' => 'Menu'
            ],
            'messages' => [
                'system-messages-3623' => 'System Messages'
            ],
            'content' => [
                'system-content-8687' => 'Page Content'
            ],
            'copyright' => [
                'copyright-3536' => 'Copyright'
            ],
            'spacer' => [
                'spacer-1078' => 'Spacer'
            ],
            'branding' => [
                'branding-7311' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-4846' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
