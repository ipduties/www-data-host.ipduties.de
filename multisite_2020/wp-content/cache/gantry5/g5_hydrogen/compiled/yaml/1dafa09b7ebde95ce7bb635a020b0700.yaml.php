<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/var/www/multisite_2020/wp-content/themes/g5_hydrogen/custom/config/ip_duties_home/index.yaml',
    'modified' => 1603671294,
    'data' => [
        'name' => 'ip_duties_home',
        'timestamp' => 1603671294,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/home.png',
            'name' => 'home',
            'timestamp' => 1603348134
        ],
        'positions' => [
            'footer' => 'Footer'
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'feature' => 'Feature',
            'subfeature' => 'Subfeature',
            'main' => 'Main',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-1070' => 'Logo / Image'
            ],
            'menu' => [
                'menu-5588' => 'Menu'
            ],
            'messages' => [
                'system-messages-9294' => 'System Messages'
            ],
            'sample' => [
                'sample-2' => 'Getting Started',
                'sample-3' => 'Key Features'
            ],
            'content' => [
                'system-content-1913' => 'Page Content'
            ],
            'position' => [
                'position-footer' => 'Footer'
            ],
            'copyright' => [
                'copyright-6000' => 'Copyright'
            ],
            'social' => [
                'social-4975' => 'Social'
            ],
            'branding' => [
                'branding-9184' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-3024' => 'Mobile Menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
