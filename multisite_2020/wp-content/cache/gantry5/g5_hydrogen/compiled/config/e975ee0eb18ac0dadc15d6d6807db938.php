<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1604826681,
    'checksum' => 'a37583bd2e4d81e50cc5b06350bf76dd',
    'files' => [
        'wp-content/themes/g5_hydrogen/custom/config/leer' => [
            'assignments' => [
                'file' => 'wp-content/themes/g5_hydrogen/custom/config/leer/assignments.yaml',
                'modified' => 1604826113
            ],
            'index' => [
                'file' => 'wp-content/themes/g5_hydrogen/custom/config/leer/index.yaml',
                'modified' => 1604826310
            ],
            'layout' => [
                'file' => 'wp-content/themes/g5_hydrogen/custom/config/leer/layout.yaml',
                'modified' => 1604826310
            ],
            'page/body' => [
                'file' => 'wp-content/themes/g5_hydrogen/custom/config/leer/page/body.yaml',
                'modified' => 1604826681
            ],
            'page/fontawesome' => [
                'file' => 'wp-content/themes/g5_hydrogen/custom/config/leer/page/fontawesome.yaml',
                'modified' => 1604826681
            ],
            'page/head' => [
                'file' => 'wp-content/themes/g5_hydrogen/custom/config/leer/page/head.yaml',
                'modified' => 1604826681
            ]
        ]
    ],
    'data' => [
        'assignments' => [
            'context' => [
                
            ],
            'menu' => [
                
            ],
            'language' => [
                
            ],
            'post' => [
                'page' => [
                    140 => true
                ]
            ],
            'taxonomy' => [
                
            ],
            'archive' => [
                
            ]
        ],
        'index' => [
            'name' => 'leer',
            'timestamp' => 1604826310,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1603348134
            ],
            'positions' => [
                
            ],
            'sections' => [
                'header' => 'Header',
                'navigation' => 'Navigation',
                'main' => 'Main',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'content' => [
                    'system-content-4144' => 'Page Content'
                ]
            ],
            'inherit' => [
                
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1603348134
            ],
            'layout' => [
                '/header/' => [
                    
                ],
                '/navigation/' => [
                    
                ],
                '/main/' => [
                    0 => [
                        0 => 'system-content-4144'
                    ]
                ],
                '/footer/' => [
                    
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'header' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'main' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'footer' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ]
            ]
        ],
        'page' => [
            'body' => [
                'attribs' => [
                    'class' => '',
                    'extra' => [
                        
                    ]
                ],
                'layout' => [
                    'sections' => '2'
                ]
            ],
            'fontawesome' => [
                'enable' => '0'
            ],
            'head' => [
                'atoms' => [
                    
                ]
            ]
        ]
    ]
];
