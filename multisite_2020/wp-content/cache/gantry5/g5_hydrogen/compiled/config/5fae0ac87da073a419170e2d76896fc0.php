<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1604826116,
    'checksum' => '0f37bfc02f414d4dfc4ca26233eca4c9',
    'files' => [
        0 => [
            'leer' => [
                'file' => 'wp-content/themes/g5_hydrogen/custom/config/leer/assignments.yaml',
                'modified' => 1604826113
            ],
            'ip_duties_home' => [
                'file' => 'wp-content/themes/g5_hydrogen/custom/config/ip_duties_home/assignments.yaml',
                'modified' => 1603349841
            ],
            'home' => [
                'file' => 'wp-content/themes/g5_hydrogen/custom/config/home/assignments.yaml',
                'modified' => 1603349299
            ]
        ]
    ],
    'data' => [
        'leer' => [
            'context' => [
                
            ],
            'menu' => [
                
            ],
            'language' => [
                
            ],
            'post' => [
                'page' => [
                    140 => true
                ]
            ],
            'taxonomy' => [
                
            ],
            'archive' => [
                
            ]
        ],
        'ip_duties_home' => [
            'context' => [
                0 => [
                    'is_home' => true
                ]
            ],
            'menu' => [
                
            ],
            'language' => [
                0 => [
                    'de_DE' => true
                ]
            ],
            'post' => [
                
            ],
            'taxonomy' => [
                
            ],
            'archive' => [
                
            ]
        ],
        'home' => [
            'context' => [
                
            ],
            'menu' => [
                
            ],
            'language' => [
                
            ],
            'post' => [
                
            ],
            'taxonomy' => [
                
            ],
            'archive' => [
                
            ]
        ]
    ]
];
