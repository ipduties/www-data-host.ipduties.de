<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1604825887,
    'checksum' => '312f1a26b8be9e7434a2abe811015ba3',
    'files' => [
        'wp-content/themes/g5_hydrogen/custom/config/_body_only' => [
            'index' => [
                'file' => 'wp-content/themes/g5_hydrogen/custom/config/_body_only/index.yaml',
                'modified' => 1603348188
            ],
            'layout' => [
                'file' => 'wp-content/themes/g5_hydrogen/custom/config/_body_only/layout.yaml',
                'modified' => 1603348188
            ]
        ]
    ],
    'data' => [
        'index' => [
            'name' => '_body_only',
            'timestamp' => 1603348188,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1603348134
            ],
            'positions' => [
                'header' => 'Header',
                'breadcrumbs' => 'Breadcrumbs',
                'footer' => 'Footer'
            ],
            'sections' => [
                'header' => 'Header',
                'navigation' => 'Navigation',
                'main' => 'Main',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'logo' => [
                    'logo-8952' => 'Logo'
                ],
                'position' => [
                    'position-header' => 'Header',
                    'position-breadcrumbs' => 'Breadcrumbs',
                    'position-footer' => 'Footer'
                ],
                'menu' => [
                    'menu-4176' => 'Menu'
                ],
                'messages' => [
                    'system-messages-1087' => 'System Messages'
                ],
                'content' => [
                    'system-content-4520' => 'Page Content'
                ],
                'copyright' => [
                    'copyright-9563' => 'Copyright'
                ],
                'spacer' => [
                    'spacer-9814' => 'Spacer'
                ],
                'branding' => [
                    'branding-9226' => 'Branding'
                ],
                'mobile-menu' => [
                    'mobile-menu-9138' => 'Mobile-menu'
                ]
            ],
            'inherit' => [
                
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1603348134
            ],
            'layout' => [
                '/header/' => [
                    0 => [
                        0 => 'logo-8952 30',
                        1 => 'position-header 70'
                    ]
                ],
                '/navigation/' => [
                    0 => [
                        0 => 'menu-4176'
                    ]
                ],
                '/main/' => [
                    0 => [
                        0 => 'position-breadcrumbs'
                    ],
                    1 => [
                        0 => 'system-messages-1087'
                    ],
                    2 => [
                        0 => 'system-content-4520'
                    ]
                ],
                '/footer/' => [
                    0 => [
                        0 => 'position-footer'
                    ],
                    1 => [
                        0 => 'copyright-9563 40',
                        1 => 'spacer-9814 30',
                        2 => 'branding-9226 30'
                    ]
                ],
                'offcanvas' => [
                    0 => [
                        0 => 'mobile-menu-9138'
                    ]
                ]
            ],
            'structure' => [
                'header' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'main' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'footer' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ]
            ],
            'content' => [
                'position-header' => [
                    'attributes' => [
                        'key' => 'header'
                    ]
                ],
                'position-breadcrumbs' => [
                    'attributes' => [
                        'key' => 'breadcrumbs'
                    ]
                ],
                'position-footer' => [
                    'attributes' => [
                        'key' => 'footer'
                    ]
                ]
            ]
        ]
    ]
];
