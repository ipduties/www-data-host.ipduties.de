<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/var/www/multisite_2020/wp-content/themes/g5_hydrogen/custom/blog-2/config/_offline/index.yaml',
    'modified' => 1603685573,
    'data' => [
        'name' => '_offline',
        'timestamp' => 1603685573,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/offline.png',
            'name' => '_offline',
            'timestamp' => 1603348134
        ],
        'positions' => [
            'footer' => 'Footer'
        ],
        'sections' => [
            'header' => 'Header',
            'main' => 'Main',
            'footer' => 'Footer'
        ],
        'particles' => [
            'logo' => [
                'logo-2891' => 'Logo'
            ],
            'spacer' => [
                'spacer-8755' => 'Spacer',
                'spacer-5828' => 'Spacer'
            ],
            'messages' => [
                'system-messages-1121' => 'System Messages'
            ],
            'content' => [
                'system-content-2857' => 'Page Content'
            ],
            'position' => [
                'position-footer' => 'Footer'
            ],
            'copyright' => [
                'copyright-1177' => 'Copyright'
            ],
            'branding' => [
                'branding-1389' => 'Branding'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
