<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/var/www/multisite_2020/wp-content/themes/g5_hydrogen/custom/blog-2/config/default/index.yaml',
    'modified' => 1603685573,
    'data' => [
        'name' => 'default',
        'timestamp' => 1603685573,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1603348134
        ],
        'positions' => [
            'header' => 'Header',
            'breadcrumbs' => 'Breadcrumbs',
            'footer' => 'Footer'
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'main' => 'Main',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-1623' => 'Logo'
            ],
            'position' => [
                'position-header' => 'Header',
                'position-breadcrumbs' => 'Breadcrumbs',
                'position-footer' => 'Footer'
            ],
            'menu' => [
                'menu-8267' => 'Menu'
            ],
            'messages' => [
                'system-messages-9395' => 'System Messages'
            ],
            'content' => [
                'system-content-5738' => 'Page Content'
            ],
            'copyright' => [
                'copyright-5505' => 'Copyright'
            ],
            'spacer' => [
                'spacer-1067' => 'Spacer'
            ],
            'branding' => [
                'branding-5511' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-4122' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
