<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/var/www/multisite_2020/wp-content/themes/g5_hydrogen/custom/blog-2/config/sidebar-right/index.yaml',
    'modified' => 1603687089,
    'data' => [
        'name' => 'sidebar-right',
        'timestamp' => 1603687089,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/2-col-right.png',
            'name' => '2_column_-_right',
            'timestamp' => 1603348134
        ],
        'positions' => [
            'header' => 'Header',
            'breadcrumbs' => 'Breadcrumbs',
            'sidebar' => 'Sidebar',
            'footer' => 'Footer'
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'main' => 'Main',
            'sidebar' => 'Sidebar',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-4676' => 'Logo'
            ],
            'position' => [
                'position-header' => 'Header',
                'position-breadcrumbs' => 'Breadcrumbs',
                'position-sidebar' => 'Sidebar',
                'position-footer' => 'Footer'
            ],
            'menu' => [
                'menu-5319' => 'Menu'
            ],
            'messages' => [
                'system-messages-5818' => 'System Messages'
            ],
            'content' => [
                'system-content-1088' => 'Page Content'
            ],
            'copyright' => [
                'copyright-3400' => 'Copyright'
            ],
            'spacer' => [
                'spacer-9267' => 'Spacer'
            ],
            'branding' => [
                'branding-7333' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-8018' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
