<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/var/www/multisite_2020/wp-content/themes/g5_hydrogen/custom/blog-2/config/_body_only/index.yaml',
    'modified' => 1603685573,
    'data' => [
        'name' => '_body_only',
        'timestamp' => 1603685573,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/body-only.png',
            'name' => '_body_only',
            'timestamp' => 1603348134
        ],
        'positions' => [
            
        ],
        'sections' => [
            'main' => 'Main'
        ],
        'particles' => [
            'messages' => [
                'system-messages-3304' => 'System Messages'
            ],
            'content' => [
                'system-content-1139' => 'Page Content'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
