<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/var/www/multisite_2020/wp-content/themes/g5_hydrogen/custom/blog-2/config/sidebar-right/assignments.yaml',
    'modified' => 1603686723,
    'data' => [
        'context' => [
            
        ],
        'menu' => [
            
        ],
        'language' => [
            
        ],
        'post' => [
            'post' => [
                1 => true,
                47 => true,
                43 => true
            ],
            'post-terms' => [
                'category' => true,
                'category-1' => true,
                'post_tag' => true,
                'post_tag-3' => true,
                'post_tag-2' => true
            ],
            'topic' => [
                0 => true,
                'is_singular' => true,
                'is_archive' => true
            ]
        ],
        'taxonomy' => [
            
        ],
        'archive' => [
            
        ]
    ]
];
