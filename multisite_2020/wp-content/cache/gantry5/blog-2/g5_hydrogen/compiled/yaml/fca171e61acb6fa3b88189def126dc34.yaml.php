<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/var/www/multisite_2020/wp-content/themes/g5_hydrogen/custom/blog-2/config/home/index.yaml',
    'modified' => 1603686885,
    'data' => [
        'name' => 'home',
        'timestamp' => 1603686878,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1603348134
        ],
        'positions' => [
            'header' => 'Header',
            'breadcrumbs' => 'Breadcrumbs',
            'footer' => 'Footer'
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'main' => 'Main',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-5420' => 'Logo'
            ],
            'position' => [
                'position-position-3004' => 'Header',
                'position-position-2808' => 'Breadcrumbs',
                'position-position-2921' => 'Footer'
            ],
            'menu' => [
                'menu-7735' => 'Menu'
            ],
            'messages' => [
                'system-messages-9143' => 'System Messages'
            ],
            'content' => [
                'system-content-6278' => 'Page Content'
            ],
            'copyright' => [
                'copyright-2166' => 'Copyright'
            ],
            'spacer' => [
                'spacer-1322' => 'Spacer'
            ],
            'branding' => [
                'branding-3366' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-4315' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            'default' => [
                'header' => 'header',
                'navigation' => 'navigation',
                'main' => 'main',
                'footer' => 'footer',
                'offcanvas' => 'offcanvas',
                'logo-5420' => 'logo-1623',
                'position-position-3004' => 'position-header',
                'menu-7735' => 'menu-8267',
                'position-position-2808' => 'position-breadcrumbs',
                'system-messages-9143' => 'system-messages-9395',
                'system-content-6278' => 'system-content-5738',
                'position-position-2921' => 'position-footer',
                'copyright-2166' => 'copyright-5505',
                'spacer-1322' => 'spacer-1067',
                'branding-3366' => 'branding-5511',
                'mobile-menu-4315' => 'mobile-menu-4122'
            ]
        ]
    ]
];
