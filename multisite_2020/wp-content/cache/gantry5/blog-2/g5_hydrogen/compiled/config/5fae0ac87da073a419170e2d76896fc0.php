<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1604816245,
    'checksum' => '7a1601e2bb703ddf0e10bb252bd84b41',
    'files' => [
        0 => [
            'sidebar-right' => [
                'file' => 'wp-content/themes/g5_hydrogen/custom/blog-2/config/sidebar-right/assignments.yaml',
                'modified' => 1603686723
            ]
        ]
    ],
    'data' => [
        'sidebar-right' => [
            'context' => [
                
            ],
            'menu' => [
                
            ],
            'language' => [
                
            ],
            'post' => [
                'post' => [
                    1 => true,
                    47 => true,
                    43 => true
                ],
                'post-terms' => [
                    'category' => true,
                    'category-1' => true,
                    'post_tag' => true,
                    'post_tag-3' => true,
                    'post_tag-2' => true
                ],
                'topic' => [
                    0 => true,
                    'is_singular' => true,
                    'is_archive' => true
                ]
            ],
            'taxonomy' => [
                
            ],
            'archive' => [
                
            ]
        ]
    ]
];
