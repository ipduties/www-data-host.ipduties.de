<script type="text/javascript" data-cfasync="false">

    var __ARFPAGELABELTEXT = "<?php echo addslashes(esc_html__('Page Label', 'ARForms')); ?>";
    var __ARFSECONDPAGELABEL = "<?php echo addslashes(esc_html__('Second Page Label', 'ARForms')); ?>";
    var __ARFPAGELABELARRAY = new Array("", "<?php echo addslashes(esc_html__('First', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Second', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Third', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Fourth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Fifth', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Sixth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Seventh', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Eighth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Ninth', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Tenth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Eleventh', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Twelfth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Thirteenth', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Fourteenth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Fifteenth', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Sixteenth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Seventeenth', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Eighteenth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Nineteenth', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Twentieth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Twenty First', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Twenty Second', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Twenty Third', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Twenty Fourth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Twenty Fifth', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Twenty Sixth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Twenty Seventh', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Twenty Eighth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Twenty Ninth', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Thirtieth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Thirty First', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Thirty Second', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Thirty Third', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Thirty Fourth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Thirty Fifth', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Thirty Sixth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Thirty Seventh', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Thirty Eighth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Thirty Ninth', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Fortieth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Forty First', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Forty Second', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Forty Third', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Forty Fourth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Forty Fifth', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Forty Sixth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Forty Seventh', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Forty Eighth', 'ARForms')); ?>",
            "<?php echo addslashes(esc_html__('Forty Ninth', 'ARForms')); ?>", "<?php echo addslashes(esc_html__('Fiftieth', 'ARForms')); ?>"
            );

    __ARFMAINURL = '<?php echo $arfajaxurl ?>';

    __ARFDEFAULTTITLE = "<?php echo addslashes(esc_html__('(Click here to add text)', 'ARForms')); ?>";

    __ARFDEFAULTDESCRIPTION = "<?php echo addslashes(esc_html__('(Click here to add description or instructions)', 'ARForms')); ?>";

    __ARFDEFAULTSECTION = "<?php echo addslashes(esc_html__('(Blank Section)', 'ARForms')); ?>";

    __ARFDELETEURL = '<?php echo admin_url('admin.php?page=ARForms&err=1'); ?>';


    __ARFINVALID = '<?php global $arfsettings;
echo $arfsettings->blank_msg;
?>';


    __ARFEQUALS = '<?php echo addslashes(esc_html__('equals', 'ARForms')); ?>';
    __ARFNOTEQUALS = '<?php echo addslashes(esc_html__('not equals', 'ARForms')); ?>';
    __ARFGREATER = '<?php echo addslashes(esc_html__('greater than', 'ARForms')); ?>';
    __ARFLESS = '<?php echo addslashes(esc_html__('less than', 'ARForms')); ?>';
    __ARFCONTAIN = '<?php echo addslashes(esc_html__('contains', 'ARForms')); ?>';
    __ARFNOTCONTAIN = '<?php echo addslashes(esc_html__('not contains', 'ARForms')); ?>';
    __ARFADDRULE = '<?php echo addslashes(esc_html__('Please add one or more rules', 'ARForms')); ?>';

    __ARFSHOW = '<?php echo addslashes(esc_html__('Show','ARForms')); ?>';
    __ARFHIDE = '<?php echo addslashes(esc_html__('Hide','ARForms')); ?>';
    __ARFENBALE = '<?php echo addslashes(esc_html__('Enable','ARForms')); ?>';
    __ARFDISABLE = '<?php echo addslashes(esc_html__('Disable','ARForms')); ?>';
    __ARFSETVALUE = '<?php echo addslashes(esc_html__('Set Value of','ARForms')); ?>';

    _ARFRADIOCHKIMGMSG = '<?php echo addslashes(esc_html__('Are you sure you want to delete image', 'ARForms')); ?>';

    __ARF_BLANKMSG='<?php echo addslashes(esc_html__('This field cannot be blank', 'ARForms'));?>';
   
    __ARF_DELETE_IMAGE_TEXT = '<?php echo addslashes(esc_html__('Please, Select appropriate option','ARForms')).' <br/> '.addslashes(esc_html__('to change/delete image','ARForms')); ?>';
    __ARF_ADD_TEXT = '<?php echo addslashes(esc_html__('Add','ARForms')); ?>';
    __ARF_DELETE_TEXT = '<?php echo addslashes(esc_html__('Delete','ARForms')); ?>';

    __ARF_CL_IF_TEXT = '<?php echo addslashes(esc_html__('IF','ARForms')); ?>';
    __ARF_CL_ALL_TEXT = '<?php echo addslashes(esc_html__('All','ARForms')); ?>';
    __ARF_CL_ANY_TEXT = '<?php echo addslashes(esc_html__('Any','ARForms')); ?>';
    __ARF_CL_THAN_TEXT = '<?php echo addslashes(esc_html__('THEN','ARForms')); ?>';

    __ARF_CL_SELECT_FIELD_TEXT = '<?php echo addslashes(esc_html__('Select Field','ARForms')); ?>';

    __ARF_IS_TEXT = '<?php echo addslashes(esc_html__('is','ARForms')); ?>';

    __ARF_THEN_EMAIL_SEND_TO_TEXT = '<?php echo addslashes(esc_html__('Then Mail Send To','ARForms')); ?>';

    __ARF_THEN_REDIRECT_TO_TEXT = '<?php echo addslashes(esc_html__('Then Redirect to','ARForms') ); ?>';

    __ARF_UNTITLED_TEXT = '<?php echo addslashes(esc_html__('Untitled','ARForms')); ?>';

    __ARF_ADD_IMAGE_TEXT = '<?php echo addslashes(esc_html__('Add Image','ARForms')); ?>';

    __ARF_YES_TEXT = '<?php echo addslashes(esc_html__('Yes','ARForms')); ?>';

    __ARF_CANCEL_TEXT = '<?php echo addslashes(esc_html__('Cancel','ARForms')); ?>';

    __ARF_PAGE_BREAK_TEXT = '<?php echo addslashes(esc_html__('Page Break','ARForms')); ?>';

    __ARF_PAGE_ONLY_TEXT = '<?php echo addslashes(esc_html__('Page','ARForms')); ?>';

    __ARF_BEGIN_ONLY_TEXT = '<?php echo addslashes(esc_html__('begin','ARForms')); ?>';

    __ARF_ADD_ICON_TEXT = '<?php echo addslashes(esc_html__('Add Icon','ARForms')); ?>';

    __ARF_EXPORT_FORM_NOTE = '<?php echo addslashes(esc_html__('To export this form, first you need to save it','ARForms')); ?>.';
  
    var mycolsize = "1col";

    jQuery(document).ready(function($){
    });

    function arf_update_order(order) {

        jQuery.each(order, function (i) {
            var field_id = order[i].split('_');
            jQuery('#arf_field_order_' + field_id[1]).val(i);
        });
    }
    <?php do_action('arf_add_extra_editor_script'); ?>
</script>