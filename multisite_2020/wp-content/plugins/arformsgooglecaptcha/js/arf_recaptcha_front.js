var render_arf_captcha_v3 = function(){
    var arf_grecaptcha_site_key = jQuery("#arf_recaptcha_v3_public_key").val();
    var arf_grecaptcha_theme = jQuery("#arf_recaptcha_v3_theme").val();

    if (typeof(window['arf_recaptcha_v3']) != 'undefined' && typeof(grecaptcha) != 'undefined') {
    	grecaptcha.ready(function() {
    		grecaptcha.execute(arf_grecaptcha_site_key).then(function(arf_recaptcha_token){
    			for( var arf_grecaptcha_field_v3 in window['arf_recaptcha_v3'] ){
    				var arf_grecaptcha_fields_v3 = arf_grecaptcha_field_v3;
    				var arf_grecaptcha_size = window['arf_recaptcha_v3'][arf_grecaptcha_field_v3]['size'];
    				arf_grecaptcha_fields_v3 = grecaptcha.render(arf_grecaptcha_field_v3, {
                        'sitekey': arf_grecaptcha_site_key,
                        'theme': arf_grecaptcha_theme,
                        'size': arf_grecaptcha_size,
                    });
                    jQuery('#'+arf_grecaptcha_field_v3).val(arf_recaptcha_token).trigger('change');
    			}
    		});
    	});
    }
}
var render_arflite_captcha_v3 = function(){
    var arflite_grecaptcha_site_key = jQuery("#arflite_recaptcha_v3_public_key").val();
    var arflite_grecaptcha_theme = jQuery("#arflite_recaptcha_v3_theme").val();
    
    
    if (typeof(window['arflite_recaptcha_v3']) != 'undefined' && typeof(grecaptcha) != 'undefined') {
        grecaptcha.ready(function() {
            grecaptcha.execute(arflite_grecaptcha_site_key).then(function(arflite_recaptcha_token){
                for( var arflite_grecaptcha_field_v3 in window['arflite_recaptcha_v3'] ){
                    var arflite_grecaptcha_fields_v3 = arflite_grecaptcha_field_v3;
                    var arflite_grecaptcha_size = window['arflite_recaptcha_v3'][arflite_grecaptcha_field_v3]['size'];
                    arflite_grecaptcha_fields_v3 = grecaptcha.render(arflite_grecaptcha_field_v3, {
                        'sitekey': arflite_grecaptcha_site_key,
                        'theme': arflite_grecaptcha_theme,
                        'size': arflite_grecaptcha_size,
                    });
                    jQuery('#'+arflite_grecaptcha_field_v3).val(arflite_recaptcha_token).trigger('change');
                }
            });
        });
    }
}
var arf_reset_captcha = function(){
	var arf_grecaptcha_site_key = jQuery("#arf_recaptcha_v3_public_key").val();

	if (typeof(window['arf_recaptcha_v3']) != 'undefined' && typeof(grecaptcha) != 'undefined') {
		grecaptcha.ready(function() {
            grecaptcha.execute(arf_grecaptcha_site_key).then(function(arf_recaptcha_token) {
                for (var arf_grecaptcha_field_v3 in window['arf_recaptcha_v3']) {
                    jQuery('#'+arf_grecaptcha_field_v3).val(arf_recaptcha_token).trigger('change');
                }
            });  
        });
	}
}
var arflite_reset_captcha = function(){
    var arflite_grecaptcha_site_key = jQuery("#arflite_recaptcha_v3_public_key").val();

    if (typeof(window['arflite_recaptcha_v3']) != 'undefined' && typeof(grecaptcha) != 'undefined') {
        grecaptcha.ready(function() {
            grecaptcha.execute(arflite_grecaptcha_site_key).then(function(arflite_recaptcha_token) {
                for (var arflite_grecaptcha_field_v3 in window['arflite_recaptcha_v3']) {
                    jQuery('#'+arflite_grecaptcha_field_v3).val(arflite_recaptcha_token).trigger('change');
                }
            });  
        });
    }
}