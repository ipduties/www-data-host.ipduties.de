��          \       �       �   �   �      P     a  Z   w  ]   �     0     3  �   7  
  1     <     X  �   q  �   �     �     �   ARForms Google reCaptcha Add-on is now uses reCaptcha v3. The current keys you are using are valid only for v2. So, you need to acquire new keys for reCaptcha v3 and update it %s Enable reCaptcha Google reCaptcha (v3) Google reCaptcha add-on for ARForms requires ARForms installed with version 4.1 or higher. Google reCaptcha add-on for ARForms requires ARForms/ARFormslite plugin installed and active. No Yes Project-Id-Version: ARForms - Google reCaptcha Add-On v1.4
PO-Revision-Date: 2020-07-17 11:36:31+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: CSL v1.x ARForms Google reCaptchaアドオンは、reCaptcha v3を使用するようになりました。 現在使用しているキーは、v2でのみ有効です。 したがって、reCaptcha v3の新しいキーを取得して、％sを更新する必要があります reCaptchaを有効にする Google reCaptcha（v3） ARForms用のGoogle reCaptchaアドオンには、バージョン4.1以降でインストールされたARFormsが必要です。 ARForms用のGoogle reCaptchaアドオンには、ARForms / ARFormsliteプラグインがインストールされ、アクティブになっている必要があります。 番号 はい 