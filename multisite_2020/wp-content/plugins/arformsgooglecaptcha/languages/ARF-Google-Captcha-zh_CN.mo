��          \       �       �   �   �      P     a  Z   w  ]   �     0     3  �   7  �   1     �     �  T     \   b     �     �   ARForms Google reCaptcha Add-on is now uses reCaptcha v3. The current keys you are using are valid only for v2. So, you need to acquire new keys for reCaptcha v3 and update it %s Enable reCaptcha Google reCaptcha (v3) Google reCaptcha add-on for ARForms requires ARForms installed with version 4.1 or higher. Google reCaptcha add-on for ARForms requires ARForms/ARFormslite plugin installed and active. No Yes Project-Id-Version: ARForms - Google reCaptcha Add-On v1.4
PO-Revision-Date: 2020-07-17 11:40:04+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=1;
X-Generator: CSL v1.x ARForms Google reCaptcha加载项现在使用reCaptcha v3。 您正在使用的当前密钥仅对v2有效。 因此，您需要获取reCaptcha v3的新密钥并对其进行更新 启用验证码 Google reCaptcha（v3） 用于ARForms的Google reCaptcha加载项需要安装4.1或更高版本的ARForms。 用于ARForms的Google reCaptcha加载项需要安装并激活ARForms / ARFormslite插件。 没有 是 