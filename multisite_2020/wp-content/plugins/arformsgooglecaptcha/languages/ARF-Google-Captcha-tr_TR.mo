��          \       �       �   �   �      P     a  Z   w  ]   �     0     3  �   7  �   1     
     %  h   B  t   �           '   ARForms Google reCaptcha Add-on is now uses reCaptcha v3. The current keys you are using are valid only for v2. So, you need to acquire new keys for reCaptcha v3 and update it %s Enable reCaptcha Google reCaptcha (v3) Google reCaptcha add-on for ARForms requires ARForms installed with version 4.1 or higher. Google reCaptcha add-on for ARForms requires ARForms/ARFormslite plugin installed and active. No Yes Project-Id-Version: ARForms - Google reCaptcha Add-On v1.4
PO-Revision-Date: 2020-07-17 11:39:34+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: CSL v1.x ARForms Google reCaptcha Eklentisi artık reCaptcha v3 kullanıyor. Kullandığınız geçerli tuşlar yalnızca v2 için geçerlidir. Yani, reCaptcha v3 için yeni anahtarlar edinmeniz ve% s güncellemeniz gerekiyor ReCaptcha'yı etkinleştir Google reCaptcha (sürüm 3) ARForms için Google reCaptcha eklentisi, 4.1 veya sonraki bir sürümle yüklenmiş ARForms gerektirir. ARForms için Google reCaptcha eklentisi, ARForms / ARFormslite eklentisinin yüklü ve etkin olmasını gerektirir. Hayır Evet 