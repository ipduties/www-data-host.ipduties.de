<?php
/*
    Plugin Name: ARForms - Google reCaptcha Add-On
    Description: Extension for ARForms plugin to provide google captcha facility. ( This plugin will work with ARForms plugin only. )
    Version: 1.4
    Plugin URI: http://www.arformsplugin.com/
    Author: Repute InfoSystems
    Author URI: http://reputeinfosystems.com/
    Text Domain: ARF-Google-Captcha
 */

if(!defined('ARF_GGL_CAPT_DIR')){
    define('ARF_GGL_CAPT_DIR', WP_PLUGIN_DIR . '/arformsgooglecaptcha');
}

global $arfsiteurl;
$arfsiteurl = home_url();
if (is_ssl() and ( !preg_match('/^https:\/\/.*\..*$/', $arfsiteurl) or ! preg_match('/^https:\/\/.*\..*$/', WP_PLUGIN_URL))) {
    $arfsiteurl = str_replace('http://', 'https://', $arfsiteurl);
    define('ARF_GGL_CAPT_URL', str_replace('http://', 'https://', WP_PLUGIN_URL . '/arformsgooglecaptcha'));
} else{
    define('ARF_GGL_CAPT_URL', WP_PLUGIN_URL . '/arformsgooglecaptcha');
}

if(!defined('ARF_GC_PATH')){
    define('ARF_GC_PATH', WP_PLUGIN_DIR . '/arformsgooglecaptcha');
}

if(!defined('ARF_GC_CORE')){
    define('ARF_GC_CORE', ARF_GC_PATH . '/core');
}

if(!defined('ARF_GC_SLUG')){
    define('ARF_GC_SLUG', 'captcha');
}

load_plugin_textdomain('ARF-Google-Captcha', false, 'arformsgooglecaptcha/languages/');

global $arf_google_captcha;
$arf_google_captcha = new ARF_Google_Captcha();

$arfgc_memory_limit = 256;
$memory_limit = ini_get("memory_limit");
if (isset($memory_limit)) {
    $memory_limit = str_replace('M', '', $memory_limit);
}

global $arf_ggl_capt_version;
$arf_ggl_capt_version = '1.4';

global $arformsgooglecaptchashortname;
$arformsgooglecaptchashortname = 'ARFGGLCAPT';

$arfajaxurl = admin_url('admin-ajax.php');

class ARF_Google_Captcha {

    function __construct() {

        global $gc_arforms_version_compatible;
        $gc_arforms_version_compatible = $this->gc_arforms_version_compatible();

        register_activation_hook(__FILE__, array('ARF_Google_Captcha', 'install'));

        register_uninstall_hook(__FILE__, array('ARF_Google_Captcha', 'uninstall'));

        add_action('admin_notices', array($this, 'arf_ggl_capt_admin_notices'));

        if( $this->gc_arformslite_version_compatible() ){

            add_action('arflite_add_form_additional_input_settings', array( $this, 'arf_enable_grecaptcha_v3'),10,2);
            
            add_filter('arflite_save_form_options_outside', array($this, 'arf_save_recaptcha_options'), 10, 2);

            add_action('wp_footer', array( $this, 'arflite_add_grecaptcha_js') );

            add_filter( 'arflite_additional_form_content_outside', array( $this, 'arflite_add_captcha_v3_inputs'), 10, 5 );

            add_action( 'arflite_update_global_setting_outside', array( $this, 'arf_update_recaptcha_global_option'),13,2);            
        }

        add_action('admin_init',array($this,'upgrade_arformsgooglecaptcha_data'), 9);

        add_action('arf_add_form_additional_input_settings', array($this,'arf_enable_grecaptcha_v3'), 10, 2);

        add_filter('arf_save_form_options_outside', array($this, 'arf_save_recaptcha_options'), 10, 2);

        add_action( 'wp_footer', array($this, 'arf_add_grecaptcha_js') );

        add_filter( 'arf_additional_form_content_outside', array( $this, 'arf_add_captcha_v3_inputs'), 10, 5 );

        add_filter('arf_update_global_setting_outside',  array($this, 'arf_update_recaptcha_global_option'),13,2);

    }

    function upgrade_arformsgooglecaptcha_data(){

        global $newarfcaptcha_version;

        if(!isset($newarfcaptcha_version) || $newarfcaptcha_version == '' ){
            $newarfcaptcha_version = get_option('arf_ggl_capt_version');
        }

        if( version_compare($newarfcaptcha_version, '1.4', '<') ){
            $path = ARF_GC_CORE.'/upgrade_latest_data.php';
            include($path);
        }
    }

    function arf_ggl_capt_admin_notices() {

    	$arf_google_captcha_notice = false;
    	$arf_google_captcha_version_notice = false;
    	$arflite_google_captcha_notice = false;

        if (!$this->is_arforms_support()){
            $arf_google_captcha_notice = true;
        } else if (!version_compare($this->get_arforms_version(), '4.1', '>=')){
            $arf_google_captcha_version_notice = true;
        }

        if( !$this->is_arformslite_support() ){
        	$arflite_google_captcha_notice = true;
        }

        if( $arflite_google_captcha_notice && $arf_google_captcha_notice ){
            echo "<div class='updated'><p>" . __('Google reCaptcha add-on for ARForms requires ARForms/ARFormslite plugin installed and active.', 'ARF-Google-Captcha') . "</p></div>";
        } else if( $arflite_google_captcha_notice && $arf_google_captcha_notice ){
            echo "<div class='updated'><p>" . __('Google reCaptcha add-on for ARForms requires ARForms installed with version 4.1 or higher.', 'ARF-Google-Captcha') . "</p></div>";
        }

        $update_key_notice = get_option('display_update_key_notice');

        if( !empty( $update_key_notice ) && 1 == $update_key_notice ){
            $link = '<a href="'. admin_url( 'admin.php?page=ARForms-settings' ) . '" target="_blank">here</a>';
        	echo "<div class='notice notice-error'><p>". sprintf( esc_html__('ARForms Google reCaptcha Add-on is now uses reCaptcha v3. The current keys you are using are valid only for v2. So, you need to acquire new keys for reCaptcha v3 and update it %s','ARF-Google-Captcha'), $link) . "</p></div>";
        }
    }

    function is_arforms_support() {
        if (file_exists( ABSPATH . 'wp-admin/includes/plugin.php' )) {
            include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        }
        return is_plugin_active('arforms/arforms.php');
    }

    function is_arformslite_support(){
    	if( file_exists( ABSPATH . 'wp-admin/includes/plugin.php' ) ){
    		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    	}

    	return is_plugin_active('arformslite/arformslite.php');
    }

    public static function install() {
        $arf_db_google_captcha_version = get_option('arf_ggl_capt_version');
        if (!isset($arf_db_google_captcha_version) || $arf_db_google_captcha_version == '') {
            global $arf_ggl_capt_version;
            update_option('arf_ggl_capt_version', $arf_ggl_capt_version);
        }
    }

    public static function uninstall() {
        global $wpdb;
        if (is_multisite()) {
            $blogs = $wpdb->get_results("SELECT blog_id FROM {$wpdb->blogs}", ARRAY_A);
            if ($blogs) {
                foreach ($blogs as $blog) {
                    switch_to_blog($blog['blog_id']);
                    delete_option('arf_ggl_capt_version');
                }
                restore_current_blog();
            }
        } else {
            delete_option('arf_ggl_capt_version');
        }
    }

    function arfgglrecaptcha_getapiurl() {
        $api_url = 'https://www.arpluginshop.com/';
        return $api_url;
    }

    function arfgglrecaptcha_get_remote_post_params($plugin_info = "") {
        global $wpdb, $arfversion;

        $action = "";
        $action = $plugin_info;

        if (!function_exists('get_plugins')) {
            require_once(ABSPATH . 'wp-admin/includes/plugin.php');
        }
        $plugin_list = get_plugins();
        $site_url = home_url();
        $plugins = array();

        $active_plugins = get_option('active_plugins');

        foreach ($plugin_list as $key => $plugin) {
            $is_active = in_array($key, $active_plugins);


            if (strpos(strtolower($plugin["Title"]), "arformsgooglecaptcha") !== false) {
                $name = substr($key, 0, strpos($key, "/"));
                $plugins[] = array("name" => $name, "version" => $plugin["Version"], "is_active" => $is_active);
            }
        }
        $plugins = json_encode($plugins);


        $theme = wp_get_theme();
        $theme_name = $theme->get("Name");
        $theme_uri = $theme->get("ThemeURI");
        $theme_version = $theme->get("Version");
        $theme_author = $theme->get("Author");
        $theme_author_uri = $theme->get("AuthorURI");

        $im = is_multisite();
        $sortorder = get_option("arfSortOrder");

        $post = array("wp" => get_bloginfo("version"), "php" => phpversion(), "mysql" => $wpdb->db_version(), "plugins" => $plugins, "tn" => $theme_name, "tu" => $theme_uri, "tv" => $theme_version, "ta" => $theme_author, "tau" => $theme_author_uri, "im" => $im, "sortorder" => $sortorder);

        return $post;
    }

    function get_arforms_version() {

        $arf_db_version = get_option('arf_db_version');

        return (isset($arf_db_version)) ? $arf_db_version : 0;
    }

    function get_arformslite_version(){
        $arflite_db_version = get_option('arflite_db_version' );

        return  isset( $arflite_db_version ) ? $arflite_db_version : 0;
    }

    function gc_check_arforms_support() {

        if (!version_compare($this->get_arforms_version(), '3.0', '>=')) {
            return true;
        } else {
            return false;
        }
    }

    function gc_arformslite_version_compatible(){
        if( version_compare( $this->get_arformslite_version(), '1.0', '>='  ) ){
            return true;
        }
    }

    function gc_arforms_version_compatible() {
        if (version_compare($this->get_arforms_version(), '4.1', '>='))
        {
            return true;
        } else {
            return false;
        }
    }
    function arf_enable_grecaptcha_v3($id,$values){
        global $arfsettings;
        $sitekey = !empty($arfsettings->pubkey) ? $arfsettings->pubkey : '';
        $secretkey = !empty($arfsettings->privkey) ? $arfsettings->privkey : '';

        if($_REQUEST['page'] == 'ARForms-Lite'){
            global $arflitesettings;
            $sitekey = !empty($arflitesettings->pubkey) ? $arflitesettings->pubkey : '';
            $secretkey = !empty($arflitesettings->privkey) ? $arflitesettings->privkey : '';
        }
    ?>
    <div class="arf_accordion_container_row_separator"></div>
    <div class="arf_accordion_container_row arf_padding">
        <div class="arf_accordion_outer_title"><?php echo addslashes(esc_html__('Google reCaptcha (v3)', 'ARF-Google-Captcha')); ?></div>
    </div>
    <div class="arf_accordion_container_row arf_half_width">
        <div class="arf_accordion_inner_title arf_two_row_text" style="padding-right: 0; width: 42%;"><?php echo addslashes(esc_html__('Enable reCaptcha', 'ARF-Google-Captcha')); ?></div>
        <div class="arf_accordion_content_container" style="width: 58%;">
            <div class="arf_float_right" style="margin-right:5px;">
                <label class="arf_js_switch_label">
                    <span class=""><?php esc_html_e('No','ARF-Google-Captcha'); ?>&nbsp;</span>
                </label>

                <span class="arf_js_switch_wrapper">
                   <input type="checkbox" class="js-switch chkstanard" <?php echo(empty($sitekey) && empty($secretkey)) ? 'disabled="disabled"' : '' ?> name="options[arf_enable_recaptcha]" id="arf_enable_recaptcha" <?php !empty( $values['arf_enable_recaptcha'] ) ? checked($values['arf_enable_recaptcha'],1) : ''; ?> value="1">
                    <span class="arf_js_switch"></span>
                </span>
                <label class="arf_js_switch_label">
                    <span class="">&nbsp;<?php esc_html_e('Yes','ARF-Google-Captcha'); ?></span>
                </label>
            </div>
        </div>
    </div>
    <?php
    }

    function arf_save_recaptcha_options($options,$values){

        $options['arf_enable_recaptcha'] = isset($values['options']['arf_enable_recaptcha']) ? $values['options']['arf_enable_recaptcha'] : '';

        return $options;
    }

    function arf_add_captcha_v3_inputs( $arf_form, $form, $arf_data_uniq_id, $arfbrowser_name, $browser_info ){
	   $enable_captcha_v3 = false;
        if( isset( $form ) && !empty( $form->options['arf_enable_recaptcha'] ) ){
            $enable_captcha_v3 = ( 1 == $form->options['arf_enable_recaptcha'] ) ? true : false;
        }

        if( $enable_captcha_v3 ){
            global $arfsettings, $arf_form_all_footer_js;
            if( !empty( $arfsettings->pubkey ) && !empty( $arfsettings->privkey ) ){

                $re_lang = !empty( $arfsettings->re_lang ) ? $arfsettings->re_lang : 'en';
                $re_theme = !empty( $arfsettings->re_theme ) ? $arfsettings->re_theme : 'light';
                $dsize = 'normal';

                $arf_form .= '<input type="hidden" id="arf_recaptcha_v3_public_key" value="' . $arfsettings->pubkey . '" />';
                $arf_form .= '<input type="hidden" id="arf_recaptcha_v3_private_key" value="' . $arfsettings->privkey . '" />';
                $arf_form .= '<input type="hidden" id="arf_recaptcha_v3_theme" value="' . $re_theme . '" />';
                $arf_form .= '<input type="hidden" id="arf_recaptcha_v3_lang" value="' . $re_lang . '" />';
                $arf_form .= '<input type="hidden" name="arf_captcha_' . $arf_data_uniq_id .'" id="arf_captcha_' . $arf_data_uniq_id .'" class="arf_required" value="" />';

                $arf_form .= '<script type="text/javascript">';
                    $arf_form .= 'window.addEventListener("DOMContentLoaded", function() { (function($) {
                                jQuery(document).ready(function (){
                            if( !window["arf_recaptcha_v3"] ){
                                window["arf_recaptcha_v3"] = {};
                            }
                            
                            window["arf_recaptcha_v3"]["arf_captcha_'.$arf_data_uniq_id.'"] = {
                                size : "' . $dsize . '"
                            };
                            
                            }); })(jQuery); });';
                $arf_form .= '</script>';
            }
        }

        return $arf_form;

    }

    function arflite_add_captcha_v3_inputs( $arf_form, $form, $arf_data_uniq_id, $arfbrowser_name, $browser_info ){
        $enable_captcha_v3 = false;
        if( isset( $form ) && !empty( $form->options['arf_enable_recaptcha'] ) ){
            $enable_captcha_v3 = ( 1 == $form->options['arf_enable_recaptcha'] ) ? true : false;
        }

        if( $enable_captcha_v3 ){
            global $arflitesettings, $arf_form_all_footer_js;
            if( !empty( $arflitesettings->pubkey ) && !empty( $arflitesettings->privkey ) ){

                $re_lang = !empty( $arflitesettings->re_lang ) ? $arflitesettings->re_lang : 'en';
                $re_theme = !empty( $arflitesettings->re_theme ) ? $arflitesettings->re_theme : 'light';
                $dsize = 'normal';

                $arf_form .= '<input type="hidden" id="arflite_recaptcha_v3_public_key" value="' . $arflitesettings->pubkey . '" />';
                $arf_form .= '<input type="hidden" id="arflite_recaptcha_v3_private_key" value="' . $arflitesettings->privkey . '" />';
                $arf_form .= '<input type="hidden" id="arflite_recaptcha_v3_theme" value="' . $re_theme . '" />';
                $arf_form .= '<input type="hidden" id="arflite_recaptcha_v3_lang" value="' . $re_lang . '" />';
                $arf_form .= '<input type="hidden" name="arflite_captcha_' . $arf_data_uniq_id .'" id="arflite_captcha_' . $arf_data_uniq_id .'" class="arf_required" value="" />';

                $arf_form .= '<script type="text/javascript">';
                    $arf_form .= 'window.addEventListener("DOMContentLoaded", function() { (function($) {
                                jQuery(document).ready(function (){
                            if( !window["arflite_recaptcha_v3"] ){
                                window["arflite_recaptcha_v3"] = {};
                            }
                            
                            window["arflite_recaptcha_v3"]["arflite_captcha_'.$arf_data_uniq_id.'"] = {
                                size : "' . $dsize . '"
                            };
                            
                            }); })(jQuery); });';
                $arf_form .= '</script>';

            }
        }

        return $arf_form;

    }

    function arf_add_grecaptcha_js(){

        global $arfforms_loaded,$arfsettings,$arfversion, $arf_form_all_footer_js;

        if(is_array($arfforms_loaded)){

	        foreach ($arfforms_loaded as $form) {

	            if (!is_object($form)){
	                continue;
	            }

	            $form->options = maybe_unserialize($form->options);

	            if(isset($form->options['arf_enable_recaptcha']) && '1' == $form->options['arf_enable_recaptcha']){

	                if(!empty($arfsettings->pubkey) && !empty($arfsettings->privkey)){

	                    wp_enqueue_script( 'arf-google-recaptcha-front', ARF_GGL_CAPT_URL.'/js/arf_recaptcha_front.js', array(), $arfversion );
	                    
	                    $arf_google_recaptcha_url = add_query_arg(
	                        array(
	                            'hl'=> $arfsettings->re_lang,
	                            'render' => $arfsettings->pubkey,
	                            'onload'=>'render_arf_captcha_v3'
	                        ),
	                        'https://www.google.com/recaptcha/api.js'
	                    );
	                    wp_enqueue_script('arf-google-recaptcha-v3',$arf_google_recaptcha_url, array('jquery'), $arfversion);
	                }
	            }
	        }
	    }
    }

    function arflite_add_grecaptcha_js(){

        global $arfliteversion, $arflite_forms_loaded,$arflitesettings;

        if(is_array($arflite_forms_loaded)){

        	foreach ($arflite_forms_loaded as $form) {

	            if (!is_object($form)){
	                continue;
	            }
	            
	            $form->options = maybe_unserialize($form->options);

	            if(isset($form->options['arf_enable_recaptcha']) && '1' == $form->options['arf_enable_recaptcha']){

	                if(!empty($arflitesettings->pubkey) && !empty($arflitesettings->privkey)){

	                    wp_enqueue_script( 'arf-google-recaptcha-front', ARF_GGL_CAPT_URL.'/js/arf_recaptcha_front.js', array(), $arfliteversion );
	        
	                    $arflite_google_recaptcha_url = add_query_arg(
	                        array(
	                            'hl'=> $arflitesettings->re_lang,
	                            'render' => $arflitesettings->pubkey,
	                            'onload'=>'render_arflite_captcha_v3'
	                        ),
	                        'https://www.google.com/recaptcha/api.js'
	                    );

	                    wp_enqueue_script('arflite-google-recaptcha-v3',$arflite_google_recaptcha_url, array('jquery'), $arfliteversion);
	                }
            	}
        	}
        }
    }

    function arf_update_recaptcha_global_option($opt_data_from_outside,$params){

        if( !empty( get_option('arf_check_recaptcha_key' ) ) ){
	        global $arfsettings;
	    	$siteKey = $arfsettings->pubkey;
	    	$arf_google_recaptcha_url = add_query_arg(
	            array(
	                'hl'=> 'en',
	                'render' => $siteKey,
	                'onload'=>'render_arf_captcha_v3'
	            ),
	            'https://www.google.com/recaptcha/api.js'
	        );

	    	$data = wp_remote_get( $arf_google_recaptcha_url, array(
	    		'timeout' => 5000
	    	) );
	    	
	    	if( !empty( $data['response']['code'] ) && 200 == $data['response']['code'] ){
	    		delete_option('display_update_key_notice');
	    		delete_option('arf_check_recaptcha_key');
	    	}
        } else {
        	delete_option( 'display_update_key_notice' );
        }

        return $opt_data_from_outside;
    }
}

global $arf_ggl_api_url, $arf_ggl_plugin_slug;

$arf_ggl_api_url = $arf_google_captcha->arfgglrecaptcha_getapiurl();
$arf_ggl_plugin_slug = basename(dirname(__FILE__));

add_filter('pre_set_site_transient_update_plugins', 'arfgglrecaptcha_check_for_plugin_update');

function arfgglrecaptcha_check_for_plugin_update($checked_data) {
    global $arf_ggl_api_url, $arf_ggl_plugin_slug, $wp_version, $arf_google_captcha, $arf_ggl_capt_version;


    if (empty($checked_data->checked))
        return $checked_data;

    $args = array(
        'slug' => $arf_ggl_plugin_slug,
        'version' => $arf_ggl_capt_version,
        'other_variables' => $arf_google_captcha->arfgglrecaptcha_get_remote_post_params(),
    );

    $request_string = array(
        'body' => array(
            'action' => 'basic_check',
            'request' => serialize($args),
            'api-key' => md5(home_url())
        ),
        'user-agent' => 'ARFGGLRECAPTCHA-WordPress/' . $wp_version . '; ' . home_url()
    );


    $raw_response = wp_remote_post($arf_ggl_api_url, $request_string);

    if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200))
        $response = @unserialize($raw_response['body']);

    if (isset($response) && !empty($response) && isset($response->token) && $response->token != "")
    {
        update_option('arfgglrecaptcha_update_token', $response->token);
    }

    if (isset($response) && is_object($response) && is_object($checked_data) && !empty($response))
        $checked_data->response[$arf_ggl_plugin_slug . '/' . $arf_ggl_plugin_slug . '.php'] = $response;

    return $checked_data;
}

add_filter('plugins_api', 'arfgglrecaptcha_plugin_api_call', 10, 3);

function arfgglrecaptcha_plugin_api_call($def, $action, $args) {
    global $arf_ggl_plugin_slug, $arf_ggl_api_url, $wp_version;

    if (!isset($args->slug) || ($args->slug != $arf_ggl_plugin_slug))
        return false;


    $plugin_info = get_site_transient('update_plugins');
    $current_version = $plugin_info->checked[$arf_ggl_plugin_slug . '/' . $arf_ggl_plugin_slug . '.php'];
    $args->version = $current_version;

    $request_string = array(
        'body' => array(
            'action' => $action,
            'update_token' => get_site_option('arfgglrecaptcha_update_token'),
            'request' => serialize($args),
            'api-key' => md5(home_url())
        ),
        'user-agent' => 'ARFGGLRECAPTCHA-WordPress/' . $wp_version . '; ' . home_url()
    );

    $request = wp_remote_post($arf_ggl_api_url, $request_string);

    if (is_wp_error($request)) {
        $res = new WP_Error('plugins_api_failed', 'An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>', $request->get_error_message());
    } else {
        $res = maybe_unserialize($request['body']);

        if ($res === false)
            $res = new WP_Error('plugins_api_failed', 'An unknown error occurred', $request['body']);
    }

    return $res;
}