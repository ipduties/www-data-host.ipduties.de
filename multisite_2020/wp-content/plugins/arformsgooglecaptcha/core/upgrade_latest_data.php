<?php

global $wpdb, $MdlDb, $arfsettings;

if( version_compare( $newarfcaptcha_version, '1.3', '<') ){

	$all_captcha_fields = $wpdb->get_results( $wpdb->prepare( "SELECT id,form_id FROM `" . $MdlDb->fields ."` WHERE type = %s GROUP BY form_id", 'captcha' ) );
	
	if( !empty( $all_captcha_fields ) ){
		foreach( $all_captcha_fields as $captcha_field ){
			$form_id = $captcha_field->form_id;

			$getOpts = $wpdb->get_row( $wpdb->prepare( "SELECT options FROM `" . $MdlDb->forms . "` WHERE form_id = %d", $form_id ) );

			if( !empty( $getOpts->options ) ){
				$form_opts = maybe_unserialize( $getOpts->options );

				$form_opts['arf_enable_captcha'] = 1;

				$form_opts_updated = maybe_serialize( $form_opts );

				$wpdb->update(
					$MdlDb->forms,
					array(
						'options' => $form_opts_updated
					),
					array(
						'id' => $form_id
					)
				);
			}
		}
	}
	
	update_option('display_update_key_notice', 1);
	if( !empty( $arfsettings->pubkey ) && !empty( $arfsettings->privkey ) ){
		update_option('arf_check_recaptcha_key', 1);
	}

}

update_option('arf_ggl_capt_version','1.4');

global $newarfcaptcha_version;

$newarfcaptcha_version = '1.4';

?>