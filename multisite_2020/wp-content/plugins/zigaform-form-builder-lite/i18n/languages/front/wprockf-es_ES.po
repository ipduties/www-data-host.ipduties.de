# Copyright (C) 2020 Zigaform WP Cost Estimation Form Builder 5.0.5
# This file is distributed under the same license as the Zigaform WP Cost Estimation Form Builder 5.0.5 package.
msgid ""
msgstr ""
"Project-Id-Version: Rocket form\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: softdiscover <info@softdiscover.com>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.4.1\n"

#: modules/formbuilder/controllers/uiform-fb-controller-frontend.php:424, modules/formbuilder/controllers/uiform-fb-controller-frontend.php:1553
msgid "warning! Form was not submitted"
msgstr "advertencia! El formulario no pudo enviarse"

#: modules/formbuilder/controllers/uiform-fb-controller-frontend.php:421, modules/formbuilder/controllers/uiform-fb-controller-frontend.php:1547
msgid "Success! your form was submitted"
msgstr "Éxito! El formulario se envió satisfactoriamente"

#: modules/formbuilder/controllers/uiform-fb-controller-frontend.php:690
msgid "Error! The file exceeds the allowed size of"
msgstr "Error! The file exceeds the allowed size of"

#: modules/formbuilder/controllers/uiform-fb-controller-frontend.php:697
msgid "Error! Type of file is not allowed to upload"
msgstr "Error! Type of file is not allowed to upload"

#: modules/formbuilder/controllers/uiform-fb-controller-frontend.php:924, modules/formbuilder/controllers/uiform-fb-controller-frontend.php:969
msgid "New form request"
msgstr "Nueva solicitud de formulario"

#: modules/formbuilder/controllers/uiform-fb-controller-records.php:368
msgid " , version : "
msgstr " , version : "

#: modules/formbuilder/controllers/uiform-fb-controller-records.php:368
msgid " , platform : "
msgstr " , platform : "

#: modules/formbuilder/views/frontend/mail_generate_fields.php:97, modules/formbuilder/views/frontend/mail_generate_fields.php:128
msgid "Units"
msgstr "Units"