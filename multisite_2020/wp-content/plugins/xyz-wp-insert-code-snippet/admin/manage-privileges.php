<?php
if ( ! defined( 'ABSPATH' ) )
    exit;

global $wp_roles;

if(!isset($_COOKIE['xyz_ics_lic_check']))
{
    $license_key=get_option('xyz_ics_license_key');
    
    if($license_key=='')
    {
        ?><br>
		
		You have to configure a valid license key to use this feature. <a href="<?php echo admin_url('admin.php?page=xyz-wp-insert-code-snippet-key');?>">Click here</a> to configure the same. If you do not have a license key, please get it from your xyzscripts <a href="http://xyzscripts.com/members/">member area</a>.
		<?php
		return;
	}
		
	else 
	{
		$new_key=$license_key;
		//validate
			
		$lic_data="";
			
		for($i=0;$i<XYZ_WP_ICS_VALIDATOR_SERVER_COUNT;$i++)
		{
			$validator = ($i == 0)?"validator":"validator".$i;
			$url = "http://".$validator.".xyzscripts.com/index.php?page=license/validate/".XYZ_WP_INSERT_CODE_PRODUCT_CODE."/".$_SERVER['HTTP_HOST']."/".$new_key;
			$lic_data="";
				
			if(ini_get('allow_url_fopen')==1 && $fp_license=fopen($url,"r"))
			{
				while(!feof($fp_license))
					$lic_data.=fgetc($fp_license);
				fclose($fp_license);
			}
			elseif (function_exists('curl_init'))
			{
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$content = curl_exec($ch);
				curl_close($ch);
				$lic_data=$content;
			}
			else
			{
				echo '<br><div style="background-color:#FF55E8;font-size:14px;font-weight:bold;width:800px;border-radius:4px; ">Either URL fopen or CURL must be enabled on your server.</div>';return;
			}
			if($lic_data== -1)
			{
				echo '<br><div style="background-color:#FF55E8;font-size:14px;font-weight:bold;width:800px;border-radius:4px; ">License validation failed. Please verify the key configured by you.</div>';return;
			}
			if($lic_data=='')
			{
				continue;
			}
			else
			{
				$expire=time()+(24*60*60);
				setcookie('xyz_ics_lic_check','1',$expire,"/");
				break;
			}			
		}
		if($lic_data=='')
		{
				echo '<br><div style="background-color:#FF55E8;font-size:14px;font-weight:bold;width:800px;border-radius:4px; ">Unable to validate license. If you are repeatedly getting this error please contact support desk.</div>';return;
		}
				
	}
}

$cap='publish_posts';
$roles = array_keys( $wp_roles->roles );

if( !is_array( $roles ) )
	$roles = array( $roles );

$hascap = array();

foreach( $roles as $role ) 
{
	if( !isset( $wp_roles->roles[$role]['capabilities'][$cap] ) || ( 1 != $wp_roles->roles[$role]['capabilities'][$cap] ) )
    	continue;
	
    $hascap[] = $role;
}

if( empty( $hascap ) )
	return false;

global $wpdb;

if(isset($_POST['role_settings']))
{
	foreach ($hascap as $role_value => $role_name) 
	{
    	if(isset($_POST['xyz_ics_premium_snippet_manage_'.$role_value]))
	    {
	    	$permission_mode_snippet_manage=$_POST['xyz_ics_premium_snippet_manage_'.$role_value];
	    	
	    	$xyz_ics_snippet_usage_setting_permission=0;
	    	//////////////////new////////////////////////////
	    	if(isset($_POST['xyz_ics_snippet_usage_setting_permission_'.$role_value]) && $_POST['xyz_ics_snippet_usage_setting_permission_'.$role_value]==1)
	    		$xyz_ics_snippet_usage_setting_permission=$_POST['xyz_ics_snippet_usage_setting_permission_'.$role_value];
	    		
	    	/////////////////new//////////////////////////////
	    		
	    	$col_exist = $wpdb->get_row( $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."xyz_ics_role_privileges WHERE role=%s and privilege=%s",array($role_name,'snippet_manage')));
	    	if(count($col_exist)>0)
	    	{
	    		$wpdb->update($wpdb->prefix."xyz_ics_role_privileges",
	    					array('value'	=>	$permission_mode_snippet_manage),
	    					array( 'id' => $col_exist->id)
	    		);
	    	}
	    	else 
	    	{
	    		$wpdb->insert($wpdb->prefix."xyz_ics_role_privileges",
	    					array('role'	=>	$role_name,
	    					'privilege'	=>	'snippet_manage',
	    					'value'	=>	$permission_mode_snippet_manage)
	    		);
	    	}
	    	
	    	$wpdb->query("DELETE FROM  ".$wpdb->prefix."xyz_ics_user_privileges WHERE privilege='snippet_manage' and role='".$role_name."'");
	    		
	    	if($permission_mode_snippet_manage==2)
	    	{
	    		$user_search_snippet_manage=$_POST['xyz_ics_premium_users_added_snippet_manage_'.$role_value];
	    		$user_search_snippet_manage=rtrim($user_search_snippet_manage,",");	    			
	    		$user_search_snippet_manage_array=explode(',', $user_search_snippet_manage);
	    			
	    		$user_name_search_snippet_manage=$_POST['xyz_ics_premium_users_added_name_snippet_manage_'.$role_value];
	    		$user_name_search_snippet_manage=rtrim($user_name_search_snippet_manage,",");
	    		$user_name_search_snippet_manage_array=explode(',', $user_name_search_snippet_manage);
	    			   				   
	    		for($i=0;$i<count($user_search_snippet_manage_array);$i++)
	    		{
	    			$user_name_search_snippet_manage_array[$i];
	    			$user_permission_exist = $wpdb->get_row( $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."xyz_ics_user_privileges WHERE user=%d and privilege=%s",array($user_search_snippet_manage_array[$i],'snippet_manage')));
	    				
	    			if(count($user_permission_exist)==0 && $user_search_snippet_manage_array[$i]!='')
	    			{
	    				$wpdb->insert($wpdb->prefix."xyz_ics_user_privileges",
	    						array('user'	=>	$user_search_snippet_manage_array[$i],
	    							'privilege'	=>	'snippet_manage',
	    							'user_name'	=>	$user_name_search_snippet_manage_array[$i],
	    							'role' => $role_name)
	    				);
	    			}
	    				 
	    		}
	    	}
	    }
	    
	    if(isset($_POST['xyz_ics_premium_snippet_usage_'.$role_value]))
	    {
	    	$permission_mode_snippet_usage=$_POST['xyz_ics_premium_snippet_usage_'.$role_value];
	    	$col_exist = $wpdb->get_row( $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."xyz_ics_role_privileges WHERE role=%s and privilege=%s",array($role_name,'snippet_usage')));
	    
	    	if(count($col_exist)>0)
	    	{
	    		$wpdb->update($wpdb->prefix."xyz_ics_role_privileges",
	    				array('value'	=>	$permission_mode_snippet_usage),
	    				array( 'id' => $col_exist->id)
	    		);
	    	}
	    	else
	    	{
	    		$wpdb->insert($wpdb->prefix."xyz_ics_role_privileges",
	    				array('role'	=>	$role_name,
	    						'privilege'	=>	'snippet_usage',
	    						'value'	=>	$permission_mode_snippet_usage)
	    		);
	    	}
	    
	    	$wpdb->query("DELETE FROM  ".$wpdb->prefix."xyz_ics_user_privileges WHERE privilege='snippet_usage' and role='".$role_name."'");
	    
	    	if($permission_mode_snippet_usage==2)
	    	{
	    		$user_search_snippet_usage=$_POST['xyz_ics_premium_users_added_snippet_usage_'.$role_value];
	    		$user_search_snippet_usage=rtrim($user_search_snippet_usage,",");
	    		$user_search_snippet_usage_array=explode(',', $user_search_snippet_usage);
	    
	    		$user_name_search_snippet_usage=$_POST['xyz_ics_premium_users_added_name_snippet_usage_'.$role_value];
	    		$user_name_search_snippet_usage=rtrim($user_name_search_snippet_usage,",");
	    		$user_name_search_snippet_usage_array=explode(',', $user_name_search_snippet_usage);
	    
	    		for($i=0;$i<count($user_search_snippet_usage_array);$i++)
	    		{
	    			$user_permission_exist = $wpdb->get_row( $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."xyz_ics_user_privileges WHERE user=%d and privilege=%s",array($user_search_snippet_usage_array[$i],'snippet_usage')));
	    
	    			if(count($user_permission_exist)==0 && $user_search_snippet_usage_array[$i]!='')
	    			{
	    				$wpdb->insert($wpdb->prefix."xyz_ics_user_privileges",
	    						array('user'	=>	$user_search_snippet_usage_array[$i],
	    							  'privilege'	=>	'snippet_usage',
	    							  'user_name'	=>	$user_name_search_snippet_usage_array[$i],
	    							  'role' => $role_name)
	    				);
	    			}
	    		}
	    	}
	    } 	
    }
    
    $xyz_ics_allow_snippet_manage_own_only=0;
    $xyz_ics_single_snippet_usage_setting_permission=0;
    
    if(isset($_POST['xyz_ics_allow_snippet_manage_own_only']))
    	$xyz_ics_allow_snippet_manage_own_only=$_POST['xyz_ics_allow_snippet_manage_own_only'];
    
    update_option('xyz_ics_allow_snippet_manage_own_only', $xyz_ics_allow_snippet_manage_own_only);
    
    
    if(isset($_POST['xyz_ics_single_snippet_usage_setting_permission']))
        $xyz_ics_single_snippet_usage_setting_permission=$_POST['xyz_ics_single_snippet_usage_setting_permission'];
        
        update_option('xyz_ics_single_snippet_usage_setting_permission', $xyz_ics_single_snippet_usage_setting_permission);

    $xyz_ics_allow_snippet_usage_own_only=0;
    	
    if(isset($_POST['xyz_ics_allow_snippet_usage_own_only']))
    	$xyz_ics_allow_snippet_usage_own_only=$_POST['xyz_ics_allow_snippet_usage_own_only'];
    	
    update_option('xyz_ics_allow_snippet_usage_own_only', $xyz_ics_allow_snippet_usage_own_only);
    	
    if(isset($_POST['xyz_ics_rm_master_pwd']))
    {
    	$xyz_ics_rm_master_pwd = $_POST['xyz_ics_rm_master_pwd'];
    	$xyz_ics_rm_master_pwd = base64_encode(($xyz_ics_rm_master_pwd));
    	update_option('xyz_ics_rm_master_pwd',$xyz_ics_rm_master_pwd);
    }
}
    
$role_permissions = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'xyz_ics_role_privileges');
    
$snippet_manage_permissions=array();
$snippet_usage_permissions=array();

foreach ($role_permissions as $role_permission)
{
	if (strcmp($role_permission->privilege, 'snippet_manage') === 0) 
	{
    	$snippet_manage_permissions[$role_permission->role][]=$role_permission->value;
       	//$snippet_manage_permissions[$role_permission->role][]=$role_permission->snippet_usage_setting_permission;
      	$snippet_usage_setting_permissions[$role_permission->role]=$role_permission->value;
    }
    if (strcmp($role_permission->privilege, 'snippet_usage') === 0) {
    	$snippet_usage_permissions[$role_permission->role]=$role_permission->value;
    }   
}
    
$xyz_ics_allow_snippet_manage_own_only=get_option('xyz_ics_allow_snippet_manage_own_only');
$xyz_ics_allow_snippet_usage_own_only=get_option('xyz_ics_allow_snippet_usage_own_only');
$xyz_ics_single_snippet_usage_setting_permission=get_option('xyz_ics_single_snippet_usage_setting_permission');
    
$xyz_ics_rm_master_pwd="";
$xyz_ics_rm_master_pwd=base64_decode(get_option('xyz_ics_rm_master_pwd'));
    
?>

<form method="post" >
	<legend><h3>Manage Privileges</h3></legend>
	<?php wp_nonce_field( 'xyz_ics_settings_form_nonce' );?>
	<table  class="widefat xyz_ics_premium_table" style="width:98%;padding-top: 10px;">
		<tr><td colspan="2"><h2>Snippet Management Privileges</h2></td><tr>
		<tr valign="top">
			<td scope="row" colspan="1">Allow edit/delete/activate/deactivate privilege for own users only</td>
			<td scope="row" colspan="1"> <input type="checkbox" <?php if($xyz_ics_allow_snippet_manage_own_only==1) echo 'checked=checked';?> name="xyz_ics_allow_snippet_manage_own_only"  value="1"/></td>
		</tr>
		
		<!-- <tr valign="top"> -->
		<!-- <td scope="row" colspan="1">Allow account creator to mange Privileges</td> -->
		<!-- <td> <input type="checkbox" <?php //if($xyz_ics_premium_og_title==1) echo 'checked=checked';?> name="xyz_ics_premium_og_title"  value="1"/></td>-->
		<!-- </tr> -->
		
		<!-- <tr><td colspan="2"><hr></td></tr> -->
		<?php 
		foreach ($hascap as $role_value => $role_name) 
		{
			$users_selected_snippet_manage=xyz_ics_get_all_selected_users('snippet_manage',$role_name);
			$snippet_manage_permission_mode=0;
			
			if(isset($snippet_manage_permissions[$role_name][0]))
				$snippet_manage_permission_mode=$snippet_manage_permissions[$role_name][0];
			
			$xyz_ics_snippet_usage_setting_permission=0;
			if(isset($snippet_manage_permissions[$role_name][1]))
				$xyz_ics_snippet_usage_setting_permission=$snippet_manage_permissions[$role_name][1];
			?>
			<tr valign="top">
				<td scope="row" colspan="1" width="30%"><?php echo $role_name;?></td>
				<td scope="row" colspan="1" width="30%">
					<input type="radio" name="xyz_ics_premium_snippet_manage_<?php echo $role_value;?>" onclick="loadSearchfield('snippet_manage',<?php echo $role_value;?>,0)" value="0" <?php  if($snippet_manage_permission_mode=='0') echo "checked"?> >
					No permission<br>
					<input type="radio" name="xyz_ics_premium_snippet_manage_<?php echo $role_value;?>" onclick="loadSearchfield('snippet_manage',<?php echo $role_value;?>,1)" value="1" <?php   if($snippet_manage_permission_mode=='1') echo "checked"?>>
					Permitted for all users in the role<br>
					<input type="radio" name="xyz_ics_premium_snippet_manage_<?php echo $role_value;?>" onclick="loadSearchfield('snippet_manage',<?php echo $role_value;?>,2)" value="2" <?php  if($snippet_manage_permission_mode=='2') echo "checked"?>>
					Permitted for specific users in the role<br>
					<div>
						<p style="margin-left:18px;margin-top:8px;display:none;" id="xyz_ics_premium_user_search_result_snippet_manage_<?php echo $role_value;?>" >
							<?php 
							$user_ids_array=explode(',',$users_selected_snippet_manage['user_ids']);
							$user_names_array=explode(',',$users_selected_snippet_manage['user_names']);
							if($users_selected_snippet_manage['user_names']!=''){
							for($m=0;$m<count($user_names_array);$m++){
								$uname=$user_names_array[$m];	
								$uid=$user_ids_array[$m];
								?>
								<span id="user_span_snippet_manage_<?php echo $role_value;?>_<?php echo $uid;?>" class='xyz_ics_user_added'><?php echo $uname;?><span>
								<span class='xyz_ics_remove_user' onclick="removeUser('<?php echo $uname;?>','snippet_manage',<?php echo $role_value;?>,<?php echo $uid;?>,'<?php echo $role_name;?>')">x</span>
								</span></span>
							<?php }}?>
						</p>
						<div style="clear:both;padding-top:5px;">
						<input style="display:none;margin-left: 23px;" value="" id="xyz_ics_premium_user_search_snippet_manage_<?php echo $role_value;?>" name="xyz_ics_premium_user_search_snippet_manage_<?php echo $role_value;?>" onkeyup="Load_UserSuggestion('snippet_manage',<?php echo $role_value;?>,'<?php echo $role_name;?>');" onblur="clearSearchbox('snippet_manage',<?php echo $role_value;?>)">  
						<input type="hidden" value="<?php echo $users_selected_snippet_manage['user_ids']?>" id="xyz_ics_premium_users_added_snippet_manage_<?php echo $role_value;?>" name="xyz_ics_premium_users_added_snippet_manage_<?php echo $role_value;?>">
						<input type="hidden" value="<?php echo $users_selected_snippet_manage['user_names']?>" id="xyz_ics_premium_users_added_name_snippet_manage_<?php echo $role_value;?>" name="xyz_ics_premium_users_added_name_snippet_manage_<?php echo $role_value;?>">
						</div>
						<div class="xyz_ics_suggestionDiv" style="display:none;" id="suggestDiv_snippet_manage_<?php echo $role_value;?>" onmouseleave="hideSuggestion('snippet_manage',<?php echo $role_value;?>)"></div>
					</div>
					<div style="display:none;" id="xyz_ics_snippet_usage_setting_permission_snippet_manage_<?php echo $role_value;?>"><input type="checkbox" <?php if($xyz_ics_snippet_usage_setting_permission==1) echo 'checked=checked';?>  name="xyz_ics_snippet_usage_setting_permission_<?php echo $role_value;?>"  value="1"/>Allow these users to set snippet usage permissions</div>
				</td>
			</tr>
			<!-- <tr><td colspan="2"><hr></td></tr> -->
		<?php }?>
		
		<tr valign="top">
			<td scope="row" colspan="1">Allow users to set snippet usage permissions</td>
			<td scope="row" colspan="1"> <input type="checkbox" <?php if($xyz_ics_single_snippet_usage_setting_permission==1) echo 'checked=checked';?> name="xyz_ics_single_snippet_usage_setting_permission"  value="1"/></td>
		</tr>
		
		<tr><td colspan="2"><h2>Snippet Usage Privileges</h2></td><tr>

		<tr valign="top">
			<td scope="row" colspan="1">Allow only owners to use snippets which do not have specific snippet usage privileges</td>
			<td> <input type="checkbox" <?php if($xyz_ics_allow_snippet_usage_own_only==1) echo 'checked=checked';?> name="xyz_ics_allow_snippet_usage_own_only" id="xyz_ics_allow_snippet_usage_own_only" value="1" onclick="displaySnippetUsagePrivileges();"/></td>
		</tr>
		<?php 
		foreach ($hascap as $role_value => $role_name) 
		{
			$users_selected_snippet_usage=xyz_ics_get_all_selected_users('snippet_usage',$role_name);
			$snippet_usage_permission_mode=0;
			
			if(isset($snippet_usage_permissions[$role_name]))
			    $snippet_usage_permission_mode=$snippet_usage_permissions[$role_name];
			?>
			<tr class="xyz_ics_genral_snippet_usage_permissions" valign="top">
				<td scope="row" colspan="1" width="30%"><?php echo $role_name;?></td>
				<td scope="row" colspan="1" width="50%">
					<input type="radio" name="xyz_ics_premium_snippet_usage_<?php echo $role_value;?>" onclick="loadSearchfield('snippet_usage',<?php echo $role_value;?>,0)" value="0" <?php  if($snippet_usage_permission_mode=='0') echo "checked"?> >
					No permission<br>
					<input type="radio" name="xyz_ics_premium_snippet_usage_<?php echo $role_value;?>" onclick="loadSearchfield('snippet_usage',<?php echo $role_value;?>,1)" value="1" <?php   if($snippet_usage_permission_mode=='1') echo "checked"?>>
					Permitted for all users in the role<br>
					<input type="radio" name="xyz_ics_premium_snippet_usage_<?php echo $role_value;?>" onclick="loadSearchfield('snippet_usage',<?php echo $role_value;?>,2)" value="2" <?php  if($snippet_usage_permission_mode=='2') echo "checked"?>>
					Permitted for specific users in the role<br>
					<div>
						<p style="margin-left:18px;margin-top:8px;display:none;" id="xyz_ics_premium_user_search_result_snippet_usage_<?php echo $role_value;?>" >
							<?php 
							$user_ids_array=explode(',',$users_selected_snippet_usage['user_ids']);
							$user_names_array=explode(',',$users_selected_snippet_usage['user_names']);
							
							if($users_selected_snippet_usage['user_names']!='')
							{
								for($m=0;$m<count($user_names_array);$m++)
								{
									$uname=$user_names_array[$m];	
									$uid=$user_ids_array[$m];
									?>
									<span id="user_span_snippet_usage_<?php echo $role_value;?>_<?php echo $uid;?>" class='xyz_ics_user_added'><?php echo $uname;?><span class='xyz_ics_remove_user' onclick="removeUser('<?php echo $uname;?>','snippet_usage',<?php echo $role_value;?>,<?php echo $uid;?>,'<?php echo $role_name;?>');" >x</span></span>
									<?php 
								}
							}
							?>
						</p>
						<div style="clear:both;padding-top:5px;">
						<input style="display:none;margin-left: 23px;" value="" id="xyz_ics_premium_user_search_snippet_usage_<?php echo $role_value;?>" name="xyz_ics_premium_user_search_snippet_usage_<?php echo $role_value;?>" onkeyup="Load_UserSuggestion('snippet_usage',<?php echo $role_value;?>,'<?php echo $role_name;?>');" onblur="clearSearchbox('snippet_usage',<?php echo $role_value;?>)">  
						<input type="hidden" value="<?php echo $users_selected_snippet_usage['user_ids']?>"id="xyz_ics_premium_users_added_snippet_usage_<?php echo $role_value;?>" name="xyz_ics_premium_users_added_snippet_usage_<?php echo $role_value;?>">
						<input type="hidden" value="<?php echo $users_selected_snippet_usage['user_names']?>" id="xyz_ics_premium_users_added_name_snippet_usage_<?php echo $role_value;?>" name="xyz_ics_premium_users_added_name_snippet_usage_<?php echo $role_value;?>">
						</div>
						<div class="xyz_ics_suggestionDiv" style="display:none;" id="suggestDiv_snippet_usage_<?php echo $role_value;?>" onmouseleave="hideSuggestion('snippet_usage',<?php echo $role_value;?>)"></div>
					</div>
				</td>
			</tr>
		<?php }?>
				
		<tr><td colspan="2" style="height:20px;"></td></tr>
		<tr valign="top">
			<td scope="row" >Master password for pages outside Role manager:</td>
			<td>
				<input id="xyz_ics_rm_master_pwd"  name="xyz_ics_rm_master_pwd" type="password" value="<?php echo $xyz_ics_rm_master_pwd;?>" />
			</td>
		</tr>
		<tr>
			<td scope="row" colspan="1" width="25%">&nbsp;</td>
		 	<td>
				<input type="submit" style="padding-left: 2px;padding-right: 2px;width:120px;" class="xyz_ics_submit_new" name="role_settings" value="Update Privileges" />
		 	</td>
		</tr>
	</table>
</form>

<script type="text/javascript">

jQuery(document).ready(function() {

	<?php 
	foreach ($hascap as $role_value => $role_name) 
	{
		$snippet_manage_permission_mode=0;
		if(isset($snippet_manage_permissions[$role_name][0]))
			$snippet_manage_permission_mode=$snippet_manage_permissions[$role_name][0];
		
		$snippet_usage_permission_mode=0;
		if(isset($snippet_usage_permissions[$role_name]))
			$snippet_usage_permission_mode=$snippet_usage_permissions[$role_name];
				
		?>

		loadSearchfield('snippet_manage',<?php echo $role_value;?>,<?php echo $snippet_manage_permission_mode;?>);
		loadSearchfield('snippet_usage',<?php echo $role_value;?>,<?php echo $snippet_usage_permission_mode;?>);
		
	<?php }?>
	
	 displaySnippetUsagePrivileges();
	 
});

function Load_UserSuggestion(permission,role,role_name)
{
	var xyz_suggestion_nonce= '<?php echo wp_create_nonce('xyz_ics_suggestion_nonce');?>';

	var search=jQuery("#xyz_ics_premium_user_search_"+permission+"_"+role).val();
	search=jQuery.trim(search);

	var searchResult=jQuery("#xyz_ics_premium_users_added_name_"+permission+"_"+role).val();
	var dataString = {
			action:'xyz_ics_load_user_suggestion',
			searchval:search,
			permission:permission,
			role:role,
			role_name:role_name,
			searchresult:searchResult,
			_wpnonce:xyz_suggestion_nonce
	};
	
	jQuery.post(ajaxurl, dataString, function(response) {
		if(response!='')
		{
			jQuery("#suggestDiv_"+permission+"_"+role).html(response);
			jQuery("#suggestDiv_"+permission+"_"+role).show();
		}
		else
		{
			jQuery("#suggestDiv_"+permission+"_"+role).hide();	
		}
	});
}

function LoadSearchValue(id,permission,role,userid,role_name)
{
	jQuery("#xyz_ics_premium_user_search_"+permission+"_"+role).val(id);

	var currentValue=jQuery("#xyz_ics_premium_users_added_name_"+permission+"_"+role).val();
	
	if(currentValue!='')
	    jQuery("#xyz_ics_premium_users_added_name_"+permission+"_"+role).val(currentValue+","+id);
	else
		jQuery("#xyz_ics_premium_users_added_name_"+permission+"_"+role).val(id+",");	

	var currentRes=jQuery("#xyz_ics_premium_user_search_result_"+permission+"_"+role).html();

	var usrdel="onclick='removeUser("+'"'+id+'","'+permission+'",'+role+','+userid+',"'+role_name+'"'+")'";
	var userLabl="<span class='xyz_ics_user_added' id='user_span_"+permission+"_"+role+"_"+userid+"'>"+id+"<span><span class='xyz_ics_remove_user' "+usrdel+" >x</span></span></span>";
	jQuery("#xyz_ics_premium_user_search_result_"+permission+"_"+role).html(currentRes+userLabl);

	var usersAdded=jQuery("#xyz_ics_premium_users_added_"+permission+"_"+role).val();

	if(usersAdded!='')
		jQuery("#xyz_ics_premium_users_added_"+permission+"_"+role).val(usersAdded+","+userid);
	else
		jQuery("#xyz_ics_premium_users_added_"+permission+"_"+role).val(userid+",");
	
	Load_UserSuggestion(permission,role,role_name);
	jQuery("#xyz_ics_premium_user_search_"+permission+"_"+role).val('');
}

function loadSearchfield(permission,role,value)
{
	if(value==2 || value==1)
   	{
		//jQuery("#xyz_ics_snippet_usage_setting_permission_"+permission+"_"+role).show();
	
		if(value==2)
		{
			jQuery("#xyz_ics_premium_user_search_"+permission+"_"+role).show();
			jQuery("#xyz_ics_premium_user_search_result_"+permission+"_"+role).show();
		}
		else
		{
			jQuery("#xyz_ics_premium_user_search_"+permission+"_"+role).hide();
			jQuery("#suggestDiv_"+permission+"_"+role).hide();
			jQuery("#xyz_ics_premium_user_search_result_"+permission+"_"+role).hide();
		}
	}
   	else
   	{
		//jQuery("#xyz_ics_snippet_usage_setting_permission_"+permission+"_"+role).hide(); 
	 	jQuery("#xyz_ics_premium_user_search_"+permission+"_"+role).hide();
		jQuery("#suggestDiv_"+permission+"_"+role).hide();
		jQuery("#xyz_ics_premium_user_search_result_"+permission+"_"+role).hide();
   	}			
}

function clearSearchbox(permission,role)
{
	searchValue=jQuery("#xyz_ics_premium_user_search_"+permission+"_"+role).val();
	
	if (searchValue.indexOf(",") >= 0)
	{
		var n = searchValue.lastIndexOf(",");
		var nsearchValue=searchValue.slice(0,n+1);
		jQuery("#xyz_ics_premium_user_search_"+permission+"_"+role).val(nsearchValue);
	}
	else
		jQuery("#xyz_ics_premium_user_search_"+permission+"_"+role).val('');
}

function hideSuggestion(permission,role)
{
	jQuery("#suggestDiv_"+permission+"_"+role).hide();
}

function removeUser(uname,permission,role,userid,role_name)
{
	var currentValue=jQuery("#xyz_ics_premium_users_added_name_"+permission+"_"+role).val();

	userNames = currentValue.replace(/(^,)|(,$)/g, "");
	if(userNames.indexOf(",")!==-1)
	{
		var userNamesArray=userNames.split(",");
		var nFlag=0;
		for (i = 0; i < userNamesArray.length; i++)
		{
			if (userNamesArray[i] === uname)
			{
				userNamesArray.splice(i, 1);
				nFlag=1;
				userNames=userNamesArray.join();
			}
		}
	}
	else if(userNames==uname)
	{
		userNames='';
	}
	jQuery("#xyz_ics_premium_users_added_name_"+permission+"_"+role).val(userNames);

	var usersAdded=jQuery("#xyz_ics_premium_users_added_"+permission+"_"+role).val();
	userIds = usersAdded.replace(/(^,)|(,$)/g, "");
	
	if(userIds.indexOf(",")!==-1)
	{
		var userIdArray=userIds.split(",");
		var iFlag=0;
		for (i = 0; i < userIdArray.length; i++)
		{
			if (userIdArray[i] == userid)
			{
				userIdArray.splice(i, 1);
				iFlag=1;
				userIds=userIdArray.join();
			}
		}
	}
	else if(userIds==userid)
	{
		userIds='';
	}
	
	jQuery("#xyz_ics_premium_users_added_"+permission+"_"+role).val(userIds);
	jQuery("#user_span_"+permission+"_"+role+"_"+userid).remove();		
}

function displaySnippetUsagePrivileges()
{
	if(document.getElementById("xyz_ics_allow_snippet_usage_own_only").checked == true) 
		jQuery(".xyz_ics_genral_snippet_usage_permissions").hide();
	else
		jQuery(".xyz_ics_genral_snippet_usage_permissions").show();
}

</script>
