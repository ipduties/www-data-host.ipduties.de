<?php
if ( ! defined( 'ABSPATH' ) )
    exit;
    add_thickbox();
    $xyz_ics_updateMessage = '';$xyz_er_msg='';
    if(isset($_GET['msg'])){
        $xyz_ics_updateMessage = esc_html($_GET['msg']);
    }
    if($xyz_ics_updateMessage == 1){
    	if(isset($_GET['error_msg']))
    		$xyz_er_msg=$_GET['error_msg'];
    		?>
    <div class="system_notice_area_style0" id="system_notice_area">
    <?php echo $xyz_er_msg;?>&nbsp;&nbsp;&nbsp;<span
    id="system_notice_area_dismiss">Dismiss</span>
    </div>
    <?php
    }
    else if($xyz_ics_updateMessage == 2){
    	?>
    <div class="system_notice_area_style0" id="system_notice_area">
    Either URL fopen or CURL must be enabled on your server.&nbsp;&nbsp;&nbsp;<span
    id="system_notice_area_dismiss">Dismiss</span>
    </div>
    <?php
    }
    else if($xyz_ics_updateMessage == 3){
    	?>
    <div class="system_notice_area_style0" id="system_notice_area">
    Please fill Email ID.&nbsp;&nbsp;&nbsp;<span
    id="system_notice_area_dismiss">Dismiss</span>
    </div>
    <?php
    }
    else if($xyz_ics_updateMessage == 4){
    	?>
    <div class="system_notice_area_style0" id="system_notice_area">
    Unable to generate license. If you are repeatedly getting this error please contact support desk.&nbsp;&nbsp;&nbsp;<span
    id="system_notice_area_dismiss">Dismiss</span>
    </div>
    <?php
    }
    else if($xyz_ics_updateMessage == 5){
    	echo '<br><div class="system_notice_area_style1" id="system_notice_area">License key updated successfully.&nbsp;&nbsp;&nbsp;<span id="system_notice_area_dismiss">Dismiss</span></div>';
    }
    else if($xyz_ics_updateMessage == 6){
        
        echo '<br><div class="system_notice_area_style0" id="system_notice_area">Automatic license key generation failed, please create key manually by logging in to your <b> <a href="https://xyzscripts.com/members/">xyzscripts  member area</a></b>.&nbsp;&nbsp;&nbsp;<span id="system_notice_area_dismiss">Dismiss</span></div>';
    }
    $ics_key=get_option('xyz_ics_license_key');
    if (isset($_POST) && isset($_POST['xyz_ics_auto_generate_lic']))
    {
    	if (! isset( $_REQUEST['_wpnonce'] )|| ! wp_verify_nonce( $_REQUEST['_wpnonce'], 'xyz_ics_auto_generate_license_form_nonce' ))
    	{
    		wp_nonce_ays( 'xyz_ics_auto_generate_license_form_nonce' );
    		exit();
    	}
    	$xyz_ics_member_email=$_POST['xyz_ics_member_email'];
    	if ($xyz_ics_member_email=='')
    		header("Location:".admin_url('admin.php?page=xyz-wp-insert-code-snippet-key&msg=3'));
    		else {
    	    if(extension_loaded('ionCube Loader'))
    		require( dirname( __FILE__ ) . '/../xyz-get-latest.php' );
    		else{
    		 header("Location:".admin_url('admin.php?page=xyz-wp-insert-code-snippet-key&msg=6'));
            exit();
    		}
    		
    		}
    	$ics_auto_generated_license_key = xyz_wp_ics_generate_license($xyz_ics_member_email); 
    	$er_str="error";
    	$ics_auto_generated_license_key_err=substr($ics_auto_generated_license_key,0,5);
    	$error_check=strcasecmp($ics_auto_generated_license_key_err,$er_str );
    	if($error_check!=0)
    	{
    	update_option('xyz_ics_license_key', $ics_auto_generated_license_key);
    	header("Location: ".admin_url('admin.php?page=xyz-wp-insert-code-snippet-key&msg=5'));
    	}
    }
$ics_key=get_option('xyz_ics_license_key');

if(isset($_POST) && isset($_POST['ics_key'] ))
{
	if (! isset( $_REQUEST['_wpnonce'] )|| ! wp_verify_nonce( $_REQUEST['_wpnonce'], 'xyz_wp_ics_license_key_form_nonce' ))
	{
		wp_nonce_ays( 'xyz_wp_ics_license_key_form_nonce' );
		exit();
	}
	$new_key=stripslashes($_POST['ics_key']);
	//validate
			
	$lic_data="";
		
	for($i=0;$i<XYZ_WP_ICS_VALIDATOR_SERVER_COUNT;$i++)
	{
		$validator = ($i == 0)?"validator":"validator".$i;
		$url = "https://".$validator.".xyzscripts.com/index.php?page=license/validate/".XYZ_WP_INSERT_CODE_PRODUCT_CODE."/".$_SERVER['HTTP_HOST']."/".$new_key;
		$lic_data="";
			
		if(ini_get('allow_url_fopen')==1 && $fp_license=fopen($url,"r"))
		{
			while(!feof($fp_license))
				$lic_data.=fgetc($fp_license);
			
			fclose($fp_license);
		}
		elseif (function_exists('curl_init'))
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$content = curl_exec($ch);
			curl_close($ch);
			$lic_data=$content;
		}
		else
		{
	       echo '<br><div class="system_notice_area_style0" id="system_notice_area">Either URL fopen or CURL must be enabled on your server.<span id="system_notice_area_dismiss">Dismiss</span></div>';
		}
		if($lic_data== -1)
		{
	       echo '<br><div class="system_notice_area_style0" id="system_notice_area">License validation failed. Please verify the key configured by you.<span id="system_notice_area_dismiss">Dismiss</span></div>';
		}
		if($lic_data=='')
		{
			continue;
		}
		else
		{
			break;
		}
	}
	if($lic_data=='')
	{
	    echo '<br><div class="system_notice_area_style0" id="system_notice_area">Unable to validate license. If you are repeatedly getting this error please contact support desk.<span id="system_notice_area_dismiss">Dismiss</span></div>';
	}
		
	if($lic_data==1)
	{
		$ics_key=$new_key;
		update_option('xyz_ics_license_key',$ics_key);
			
	    echo '<br><div class="system_notice_area_style1" id="system_notice_area">License key updated successfully.<span id="system_notice_area_dismiss">Dismiss</span></div>';
	}		
}
?>
<form method="post" >
<?php wp_nonce_field('xyz_wp_ics_license_key_form_nonce');?>
    <h2>License Configuration</h2>
    <table  align="left" style=" width: 800px;padding: 5px !important;height: 30px !important;">
        <tr valign="top" >
<td width="200px" scope="row" >License Key<span class="mandatory">*</span></td>
<td width="500px"><input type="text" name="ics_key" value="<?php echo $ics_key; ?>" style="width:300px;">
<input class="button-primary" type="submit" value="Submit">
</td></tr>
        <tr><td></td><td></tr><tr></tr>
 <tr><td></td><td>
<a style="padding-left:5px;cursor:pointer;color:#0073aa;" onclick="javascript:return xyz_ics_popup_fn_license_generate();" title="XYZ WP ICS auto generate license key" >Click here to auto generate license key</a>
</td></tr>
    </table>
</form>
<div id="ics_auto_generate_license" class="ics_auto_generate_license" style="display: none;">
<form name="ics_auto_generate_license_form" method="post" id="ics_auto_generate_license_form" >
<?php wp_nonce_field( 'xyz_ics_auto_generate_license_form_nonce' );?>
		<table class="widefat xyz_cfl_table"  style="width: 100%; margin: 0 auto; left:-50px;">
			<tr>
			<td scope="row" colspan="1">Enter your xyzscripts's member area email ID <span class="mandatory">*</span>	
        	</td>
            <td>
              <input id="xyz_ics_member_email" name="xyz_ics_member_email" type="text" />
	    </td>
        </tr>
		
        <tr>
		<td id="bottomBorderNone">&nbsp;</td>
		<td   id="bottomBorderNone" style="height: 50px">
			<input type="submit" id="xyz_ics_auto_generate_lic" class="xyz_ics_submit_new"	style=" margin-top: 10px; "	name="xyz_ics_auto_generate_lic" value="Auto generate license key" />
        	</td>
        </tr>
    </table>
</form>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
	var delayInMilliseconds = 1000; //1 second
	var ics_key='<?php echo get_option('xyz_ics_license_key');?>';
	setTimeout(function() {
		if(ics_key==='')
			xyz_ics_popup_fn_license_generate();
	}, delayInMilliseconds);
});
function xyz_ics_popup_fn_license_generate()
{
	tb_show("Auto generate license key", "#TB_inline?height=115&amp;width=600&amp;inlineId=ics_auto_generate_license&class=thickbox");
	return true;	
}
</script>
