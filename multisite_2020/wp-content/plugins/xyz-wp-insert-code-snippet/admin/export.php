<?php
if ( ! defined( 'ABSPATH' ) )
	exit;

global $wpdb;

if(!isset($_COOKIE['xyz_ics_lic_check']))
{
    $license_key=get_option('xyz_ics_license_key');
    
    if($license_key=='')
    {
        ?>
        <br>
		You have to configure a valid license key to use this feature. <a href="<?php echo admin_url('admin.php?page=xyz-wp-insert-code-snippet-key');?>">Click here</a> to configure the same. If you do not have a license key, please get it from your xyzscripts <a href="http://xyzscripts.com/members/">member area</a>.
		<?php
		return;
	}
		
	else 
	{
		$new_key=$license_key;
		//validate
			
		$lic_data="";
			
		for($i=0;$i<XYZ_WP_ICS_VALIDATOR_SERVER_COUNT;$i++)
		{
 			$validator = ($i == 0)?"validator":"validator".$i;
			$url = "http://".$validator.".xyzscripts.com/index.php?page=license/validate/".XYZ_WP_INSERT_CODE_PRODUCT_CODE."/".$_SERVER['HTTP_HOST']."/".$new_key;
			$lic_data="";
				
			if(ini_get('allow_url_fopen')==1 && $fp_license=fopen($url,"r"))
			{
				while(!feof($fp_license))
					$lic_data.=fgetc($fp_license);
				fclose($fp_license);
			}
			elseif (function_exists('curl_init'))
			{
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$content = curl_exec($ch);
				curl_close($ch);
				$lic_data=$content;
			}
			else
			{
				echo '<br><div style="background-color:#FF55E8;font-size:14px;font-weight:bold;width:800px;border-radius:4px; ">Either URL fopen or CURL must be enabled on your server.</div>';return;
			}
			if($lic_data== -1)
			{
				echo '<br><div style="background-color:#FF55E8;font-size:14px;font-weight:bold;width:800px;border-radius:4px; ">License validation failed. Please verify the key configured by you.</div>';return;
			}
			if($lic_data=='')
			{
				continue;
			}
			else
			{
				$expire=time()+(24*60*60);
				setcookie('xyz_ics_lic_check','1',$expire,"/");
				break;
			}			
		}
		if($lic_data=='')
		{
				echo '<br><div style="background-color:#FF55E8;font-size:14px;font-weight:bold;width:800px;border-radius:4px; ">Unable to validate license. If you are repeatedly getting this error please contact support desk.</div>';return;
		}
				
	}
}


$xyz_ics_message = '';

if(isset($_GET['xyz_ics_msg']))
{
	$xyz_ics_message = $_GET['xyz_ics_msg'];
}
if($xyz_ics_message == 1)
{
?>
	<div class="system_notice_area_style0" id="system_notice_area">
		Snippet not exist.&nbsp;&nbsp;&nbsp;
		<span id="system_notice_area_dismiss">Dismiss</span>
	</div>
<?php
}
if($xyz_ics_message == 2)
{
?>
	<div class="system_notice_area_style0" id="system_notice_area">
		Select snippets to export.&nbsp;&nbsp;&nbsp;
		<span id="system_notice_area_dismiss">Dismiss</span>
	</div>
<?php
}
$html_snippet_id_str="";
$php_snippet_id_str="";

$allowed_html_snippet_ids=apply_filters('xyz_ics_before_snippet_fetching', array('snippet_type'=>1,'snippet_manage'=>1));
$allowed_php_snippet_ids=apply_filters('xyz_ics_before_snippet_fetching', array('snippet_type'=>2,'snippet_manage'=>1));

if($allowed_html_snippet_ids!="")
{
    $html_snippet_id_str=" AND id in(".$allowed_html_snippet_ids['permitted_snippet_ids'].") ";
}
if($allowed_php_snippet_ids!="")
{
    $php_snippet_id_str=" AND id in(".$allowed_php_snippet_ids['permitted_snippet_ids'].") ";
}

$html_entries = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix."xyz_ics_short_code WHERE status=1 $html_snippet_id_str ORDER BY title ASC" );
$php_entries = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix."xyz_ics_short_code WHERE status=1 $php_snippet_id_str ORDER BY title ASC" );

if($_POST)
{
	if (! isset( $_REQUEST['_wpnonce'] ) || ! wp_verify_nonce( $_REQUEST['_wpnonce'], 'snipp-export_' ))
	{
		wp_nonce_ays( 'snipp-export_' );
		exit;
	}
	else
	{
		$_POST=xyz_trim_deep($_POST);
		$_POST = stripslashes_deep($_POST);

		$type=$_POST["xyz_ics_type"];
		
		if($type==1)
			$selected_html_snippets=$_POST["hid_html_selected"];
	    if($type==2)
			$selected_php_snippets=$_POST["hid_php_selected"];
	    else 
	    {
	        $selected_html_snippets=$_POST["hid_html_selected"];
	        $selected_php_snippets=$_POST["hid_php_selected"];
	    }
		
	    if(($type==1 && $selected_html_snippets=="") || ($type==2 && $selected_php_snippets==""))
		{
			header("Location:".admin_url('admin.php?page=xyz-wp-insert-code-snippet-export&xyz_ics_msg=2'));
			exit();
		}
		else if($type==0 && $selected_html_snippets=="" && $selected_php_snippets=="")
		{
		    header("Location:".admin_url('admin.php?page=xyz-wp-insert-code-snippet-export&xyz_ics_msg=2'));
		    exit();
		}
		else
		{
		    if($type==1)
		        $selected_snippets=$selected_html_snippets;
		      
		    if($type==2)
		        $selected_snippets=$selected_php_snippets;
		          
		    if($type==0)
		    {
		        $selected_snippets="";
		        
		        if($selected_html_snippets!="")
		            $selected_snippets.=$selected_html_snippets;
		        
		        if($selected_php_snippets!="")
		        {
		            if($selected_snippets!="")
		                $selected_snippets.=",";
		            
		            $selected_snippets.=$selected_php_snippets;
		        }
		    }
		    
			$query= $wpdb->get_results($wpdb->prepare("SELECT * FROM ".$wpdb->prefix."xyz_ics_short_code WHERE status=%d AND id IN($selected_snippets) ORDER BY id desc",1));
			
			if(count($query)>0)
			{
				$snippet_array=array();
				
				foreach($query as $res)
				{
					$id=$res->id ;
					$title=$res->title;
					$content=$res->content;
					$short_code=$res->short_code;
					$type=$res->snippet_type;
					
					$snippet_array[] = array('type' =>$type, 'title' =>$title, 'content' =>$content);
				}
				
				$snippet_data=json_encode($snippet_array);
				
				$upload_dir = wp_upload_dir();
				$upload_dir_path= $upload_dir['basedir']; // uploads folder
				
				//$plugin_dir_path = dirname(__FILE__);
				$fileName="snippets.txt";
				$txtfile=$upload_dir_path."/xyz_ics/export/snippets.txt";
				$handle = fopen($txtfile, 'w') or die('Cannot open file:  '.$txtfile); // check the file is readable
				fwrite($handle, $snippet_data); // write content
				fclose($handle);
				
				/*header('Content-Description: File Transfer');
	    		header('Cache-Control: public');
	    		header('Content-Type: text/plain');
	   		 	header("Content-Transfer-Encoding: binary");
	    		header('Content-Disposition: attachment; filename='. basename($fileName));
	    		header('Content-Length: '.filesize($txtfile));
	    		ob_clean(); #THIS!
	    		flush();
	    		readfile($txtfile);
				die;*/
				
				ob_clean(); #THIS!
				header('Content-Type: text/plain');
				header('Content-Disposition: attachment; filename='. basename($fileName));
				flush();
				readfile($txtfile);
				die;
				
			}
			else
			{
				header("Location:".admin_url('admin.php?page=xyz-wp-insert-code-snippet-export&xyz_ics_msg=1'));
				exit();
			}
		}
	}
}

?>

<script type="text/javascript">

jQuery(document).ready(function() {

	jQuery("#submit").click(function() {

		var xyz_ics_type=jQuery("#xyz_ics_type").val();

		if((xyz_ics_type==1 && jQuery("#hid_html_selected").val()=="") || (xyz_ics_type==2 && jQuery("#hid_php_selected").val()=="") || (xyz_ics_type==0 && jQuery("#hid_html_selected").val()=="" && jQuery("#hid_php_selected").val()==""))
		{
			alert("Select snippets to export");
			return false;
		}

		/*  
		if(xyz_ics_type==0)
		{
			alert("Please select snippet type");
			return false;
		}
		else if((xyz_ics_type==1 && jQuery("#hid_html_selected").val()=="") || (xyz_ics_type==2 && jQuery("#hid_php_selected").val()==""))
		{
			alert("Select snippets to export");
			return false;
		}
		*/
		
	});	
});

function changeType()
{
	var xyz_ics_type=jQuery("#xyz_ics_type").val();

	jQuery("#php_snippets").hide();
	jQuery("#html_snippets").hide();
	
	if(xyz_ics_type==1)
	{
		jQuery("#html_snippets").show();
		jQuery("#php_snippets").hide();
	}
	else if(xyz_ics_type==2)
	{
		jQuery("#html_snippets").hide();
		jQuery("#php_snippets").show();
	}
	else
	{
		jQuery("#html_snippets").show();
		jQuery("#php_snippets").show();
	}		
}

function select_snippet(type,id)
{
	if(type==1)
	{
		var selected="";
		
		jQuery('input[class="xyz_ics_check_html_snippet"]:checked').each(function() {
			   
			var check_id=this.id;
		    var check_id_split=check_id.split("_");
		    var snippet_id=check_id_split[2];

		    if(selected=="")
		    	selected=snippet_id;
		    else
		       	selected=selected+","+snippet_id;
		});

		jQuery("#hid_html_selected").val(selected);
	}
	else
	{
		var selected="";
		
		jQuery('input[class="xyz_ics_check_php_snippet"]:checked').each(function() {
			   
			var check_id=this.id;
		    var check_id_split=check_id.split("_");
		    var snippet_id=check_id_split[2];

		    if(selected=="")
		    	selected=snippet_id;
		    else
		       	selected=selected+","+snippet_id;
		});

		jQuery("#hid_php_selected").val(selected);
	}
}

function snippet_action(snippet_type,action_type)
{
	if(action_type==1) //check all
	{
		if(snippet_type==1)
		{
			var selected="";
			
			jQuery('#html_snippets').find('input[class="xyz_ics_check_html_snippet"]').each(function() {
				   
				jQuery(this).attr("checked", true);
			});
			  
			jQuery('input[class="xyz_ics_check_html_snippet"]:checked').each(function() {
				   
				var check_id=this.id;
			    var check_id_split=check_id.split("_");
			    var snippet_id=check_id_split[2];

			    if(selected=="")
			    	selected=snippet_id;
			    else
			       	selected=selected+","+snippet_id;
			});

			jQuery("#hid_html_selected").val(selected);
		}
		else
		{
			var selected="";
			
			jQuery('#php_snippets').find('input[class="xyz_ics_check_php_snippet"]').each(function() {
				   
				 jQuery(this).attr("checked", true);
			});		
				
			jQuery('input[class="xyz_ics_check_php_snippet"]:checked').each(function() {
				   
				var check_id=this.id;
			    var check_id_split=check_id.split("_");
			    var snippet_id=check_id_split[2];

			    if(selected=="")
			    	selected=snippet_id;
			    else
			       	selected=selected+","+snippet_id;
			});

			jQuery("#hid_php_selected").val(selected);
		}
	}
	else //uncheck all
	{
		if(snippet_type==1)
		{
			jQuery('#html_snippets').find('input[class="xyz_ics_check_html_snippet"]').each(function() {
				   
				jQuery(this).attr("checked", false);
			});
			
			jQuery("#hid_html_selected").val("");
		}
		else
		{
			jQuery('#php_snippets').find('input[class="xyz_ics_check_php_snippet"]').each(function() {
				   
				jQuery(this).attr("checked", false);
			});
			
			jQuery("#hid_php_selected").val("");
		}
	}
}
</script>

<div>
	<form method="post">
		<?php wp_nonce_field('snipp-export_');?>
		<div style="float: left;width: 98%">
			<fieldset style=" width:100%; border:1px solid #F7F7F7; padding:10px 0px 15px 10px;">
				<legend ><h3>Export Snippets</h3></legend>				
				<p>You can export snippets from this page and use the exported file to import the snippets to another wordpress.</p>
				<table class="widefat"  style="width:99%;">
					<tr valign="top">
						<td style="border-bottom: none;width:20%;">
							&nbsp;&nbsp;&nbsp;Type&nbsp;
							<font color="red">
								*
							</font>
						</td>
						<td style="border-bottom: none;width:1px;">
							&nbsp;:&nbsp;
						</td>
						<td>
							<select id="xyz_ics_type" name="xyz_ics_type" onchange="changeType();">
								<option value="0" <?php if(isset($_POST['xyz_ics_type']) && $_POST['xyz_ics_type']=='0'){echo 'selected';}  ?>>All</option>
								<option value="1" <?php if(isset($_POST['xyz_ics_type']) && $_POST['xyz_ics_type']=='1'){echo 'selected';}  ?>>HTML</option>
								<option value="2" <?php if(isset($_POST['xyz_ics_type']) && $_POST['xyz_ics_type']=='2'){ echo 'selected';}?>>PHP</option>
							</select>
						</td>
					</tr>
					<tr id="html_snippets">
						<td colspan="3"> 
							<label class="xyz_ics_snippet_list_label">Select HTML Snippets to Export</label>
							<?php if(count($html_entries)>0){?>	
								<div class="xyz_ics_check_uncheck_div">
									<span class="xyz_ics_check_link" onclick="snippet_action(1,1);">Check All</span>
									&nbsp;/&nbsp;
									<span class="xyz_ics_check_link" onclick="snippet_action(1,2);">Uncheck All</span>
								</div>
								<div style="clear:both;"></div>
							<?php }?>		
							<div class="xyz_ics_snippet_list_div">
								<?php 
								if(count($html_entries)>0)
								{
									foreach( $html_entries as $html_entry )
									{
										?>
										<div class="xyz_ics_snippet_check_div">
											<div class="xyz_ics_ckeck_div">
												<input type="checkbox" class="xyz_ics_check_html_snippet" id="html_snippet_<?php echo $html_entry->id;?>" name="html_snippet_<?php echo $html_entry->id;?>" value="1" onclick="select_snippet(1,'<?php echo $html_entry->id;?>');">
											</div>		
											<div class="xyz_ics_check_text">
												<?php echo esc_html($html_entry->title);?>
											</div>
										</div>
									<?php 
									}
								}
								else
								{
								?>
									<div>Snippets not found</div>	
								<?php }?>
							</div>
						</td>
					</tr>
					<tr id="php_snippets">
						<td colspan="3">
							<label class="xyz_ics_snippet_list_label">Select PHP Snippets to Export</label>
							<?php if(count($php_entries)>0){?>
								<div class="xyz_ics_check_uncheck_div">
									<span class="xyz_ics_check_link" onclick="snippet_action(2,1);">Check All</span>
									&nbsp;/&nbsp;
									<span class="xyz_ics_check_link" onclick="snippet_action(2,2);">Uncheck All</span>
								</div>
								<div style="clear:both;"></div>
							<?php }?>
							<div class="xyz_ics_snippet_list_div">
									<?php 
									if(count($php_entries)>0)
									{
										foreach( $php_entries as $php_entry )
										{
										?>
											<div class="xyz_ics_snippet_check_div">
												<div class="xyz_ics_ckeck_div">
													<input type="checkbox" class="xyz_ics_check_php_snippet" id="php_snippet_<?php echo $php_entry->id;?>" name="php_snippet_<?php echo $php_entry->id;?>" value="1" onclick="select_snippet(2,'<?php echo $php_entry->id;?>');">
												</div>		
												<div class="xyz_ics_check_text">
													<?php echo esc_html($php_entry->title);?>
												</div>
											</div>
										<?php 
										}
									}
									else
									{
									?>
										<div>Snippets not found</div>
									<?php }?>
							</div>
						</td>
					</tr>					
					<tr valign="top">
						<td scope="row" class="xyz_ics_settingInput"></td>
						<td colspan="2" id="bottomBorderNone">
							<input type="hidden" name="hid_html_selected" id="hid_html_selected" value="">
							<input type="hidden" name="hid_php_selected" id="hid_php_selected" value="">
							<input style="margin:10px 0 20px 30px;width:150px;" id="submit" class="button-primary xyz_ics_bottonWidth" type="submit" value="Download Export File" />
						</td>
					</tr>
				</table >
			</fieldset>
		</div>
	</form>
</div>		
