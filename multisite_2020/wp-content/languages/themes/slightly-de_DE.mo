��    "      ,  /   <      �     �     �               #  8   8     q  E   y  W   �               (  !   6  %   X     ~     �     �  F   �     �     �                    -     6  c   H  \   �     	       U        m     �     �    �     �     �     �     �     �  3        :  @   H  b   �     �     �     �     	  2   (	     [	     c	     ~	  U   �	     �	     �	     
     
     !
     5
     >
  p   U
  `   �
     '     @  6   B     y     �     �     	                 !                                                                                                    "                
                    %1$s ,  Add widgets here. Body Text Color Comments are closed. Continue reading %s <span class="meta-nav">&rarr;</span> Edit %s It looks like nothing was found at this location. Maybe try a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Menu Nick Ciliak Nothing Found One thought on &ldquo;%1$s&rdquo; Oops! That page can&rsquo;t be found. Pages: Posted in %1$s Primary Ready to publish your first post? <a href="%1$s">Get started here</a>. Search Results for: %s Sidebar Sidebar Page Site branding Skip to content Slightly Slightly Settings Slightly is a minimal, clean, ever-so-slightly opinionated WP theme for a beautifully simple start. Sorry, but nothing matched your search terms. Please try again with some different keywords. Tagged %1$s Y comments title%1$s thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; http://nickciliak.com http://slightlytheme.com post date%s  PO-Revision-Date: 2019-12-08 22:00:00+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: de
Project-Id-Version: Themes - Slightly
 %1$s ,  Die Widgets hier hinzufügen. Textfarbe für Body Kommentare sind geschlossen. Weiterlesen %s <span class="meta-nav">&rarr;</span> %s bearbeiten Hier konnte nichts gefunden werden. Vielleicht über die Suche?  Es scheint, dass wir nicht finden können wonach du suchst. Möglicherweise hilft eine neue Suche. Menü Nick Ciliak Nichts gefunden Ein Gedanke zu „%1$s“ Hoppla! Diese Seite konnte nicht gefunden werden.  Seiten: Veröffentlicht unter %1$s Primär Bereit zur Veröffentlichung des ersten Beitrags? <a href="%1$s">Hier geht's los</a>. Suchergebnisse für: %s Seitenleiste Seitenleiste Website-Identität Zum Inhalt springen Slightly Slightly-Einstellungen Slightly ist ein minimalistisches, sauberes, leicht eigenwilliges WP-Theme für einen wunderbar einfachen Start. Leider ergab deine Suchanfrage keine Ergebnisse. Bitte versuche es mit anderen Begriffen erneut. Verschlagwortet mit %1$s Y %1$s Gedanke zu „%2$s“ %1$s Gedanken zu „%2$s“ http://nickciliak.com http://slightlytheme.com %s  